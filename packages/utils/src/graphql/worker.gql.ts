import gql from 'graphql-tag';

//#region Find

export const current_worker = gql`
	query current_worker {
		current_worker {
			_id
			email
			name
			surname
			displayName
			roles
			photo
			bio
		}
	}
`;

export const worker_find = gql`
	query worker_find($paginate: PaginateDto) {
		worker_find(paginate: $paginate) {
			data {
				_id
				email
				name
				surname
				active
				photo
				roles
			}
			offset
			limit
			total
		}
	}
`;

export const worker_find_by_id = gql`
	query worker_find_by_id($workerId: ID!) {
		worker_find_by_id(workerId: $workerId) {
			_id
			email
			name
			surname
			displayName
			active
			username
			photo
			bio
			roles
			phone
			teaches
		}
	}
`;

export const worker_find_by_regex_email = gql`
	query worker_find_by_regex_email($workerEmail: String!) {
		worker_find_by_regex_email(workerEmail: $workerEmail) {
			_id
			photo
			email
			displayName
		}
	}
`;

//#endregion

//#region Login

export const worker_login = gql`
	query worker_login($input: WorkerLoginDto!) {
		worker_login(input: $input) {
			token
			user {
				_id
				email
				name
				surname
				displayName
				roles
				photo
				bio
			}
		}
	}
`;

export const worker_register = gql`
	mutation worker_register($input: WorkerRegisterDto!) {
		worker_register(input: $input)
	}
`;

export const worker_modify_data = gql`
	mutation worker_modify_data($workerId: ID!, $input: WorkerModifyDataDto!) {
		worker_modify_data(workerId: $workerId, input: $input) {
			name
		}
	}
`;

export const worker_permanent_delete = gql`
	mutation worker_permanent_delete($workerId: ID!) {
		worker_permanent_delete(workerId: $workerId)
	}
`;

//#endregion

//#region Enable

export const worker_enable = gql`
	mutation worker_enable($workerId: ID!) {
		worker_enable(workerId: $workerId)
	}
`;

export const worker_disable = gql`
	mutation worker_disable($workerId: ID!) {
		worker_disable(workerId: $workerId)
	}
`;

//#endregion

//#region Roles

export const worker_edit_roles = gql`
	mutation worker_edit_roles($workerId: ID!, $roles: [WorkerRoles]!) {
		worker_edit_roles(workerId: $workerId, roles: $roles)
	}
`;

//#endregion
