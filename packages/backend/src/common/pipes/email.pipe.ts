import { StudentErrors } from '@Students/enums/student-errors.enum';
import { BadRequestException, Injectable, PipeTransform } from '@nestjs/common';
import { FormValidation } from 'learnthis-utils';

/**
 * Validates if email has a valid format.
 */
@Injectable()
export class EmailPipe implements PipeTransform {
	/**
	 * Validation pipe
	 * @param value Email
	 * @returns Same email
	 */
	transform(value: string): string {
		if (!FormValidation.emailValidation(value))
			throw new BadRequestException(StudentErrors.FORMAT_EMAIL);
		return value;
	}
}
