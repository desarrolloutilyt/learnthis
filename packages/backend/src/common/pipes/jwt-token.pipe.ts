import { CommonErrors } from '@Common/enums/common-errors.enum';
import { BadRequestException, Injectable, PipeTransform } from '@nestjs/common';
import { TokenValidation } from 'learnthis-utils';

/**
 * Validates if a token has a valid JWT format, and it's not expired.
 */
@Injectable()
export class JwtTokenPipe implements PipeTransform {
	/**
	 * Validation pipe
	 * @param value JWT token
	 * @returns Same JWT token
	 */
	transform(value: string): string {
		if (!TokenValidation.validateJwt(value))
			throw new BadRequestException(CommonErrors.TOKEN_NOT_FOUND);

		return value;
	}
}
