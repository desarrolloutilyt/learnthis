import { CommonErrors } from '@Common/enums/common-errors.enum';
import { ObjectID } from '@Common/types/object-id.type';
import { BadRequestException, Injectable, PipeTransform } from '@nestjs/common';
import { isValidObjectId } from 'mongoose';

/**
 * Validate if object id has a valid Mongo's object id format.
 */
@Injectable()
export class ObjectIDPipe implements PipeTransform {
	/**
	 * Validation pipe
	 * @param value ObjectId
	 * @returns Same ObjectId
	 */
	transform(value: string): ObjectID {
		if (!isValidObjectId(value))
			throw new BadRequestException(CommonErrors.FORMAT_OBJECT_ID);

		return value;
	}
}
