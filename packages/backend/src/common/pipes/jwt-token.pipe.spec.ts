import { BadRequestException } from '@nestjs/common';
import { JwtTokenPipe } from '@Common/pipes/jwt-token.pipe';

describe('TokenPipe', () => {
	let tokenPipe: JwtTokenPipe;

	beforeAll(() => (tokenPipe = new JwtTokenPipe()));

	it('Token válido', () => {
		const token =
			'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyfQ.SflKxwRJSMeKKF2QT4fwpMeJf36POk6yJV_adQssw5c';
		expect(tokenPipe.transform(token)).toBe(token);
	});

	it('Token inválido', () => {
		const token =
			'eyJhbGciOiJIUzI1NiIsIn45cCI6IkpXVCJ9123JzfdseOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyfQ.SflKxwRJSMeKKF2QT4fwpMeJfghPOk6yJV_ad523w5c';
		expect(() => tokenPipe.transform(token)).toThrow(BadRequestException);
	});
});
