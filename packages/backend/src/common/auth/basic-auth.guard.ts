import { CommonErrors } from '@Common/enums/common-errors.enum';
import {
	CanActivate,
	ExecutionContext,
	Injectable,
	UnauthorizedException,
} from '@nestjs/common';
import { WorkerService } from '@Workers/services/worker.service';
import { compare } from 'bcrypt';

@Injectable()
export class BasicAuthGuard implements CanActivate {
	constructor(private readonly workerService: WorkerService) {}

	async canActivate(context: ExecutionContext): Promise<boolean> {
		const request = context.switchToHttp().getRequest();
		const b64auth = (request.headers.authorization || '').split(' ')[1] || '';
		const [username, password] = Buffer.from(b64auth, 'base64')
			.toString()
			.split(':');

		const user = await this.workerService.findByEmail(username);

		if (
			user &&
			(await compare(password, user.password)) !== false &&
			user.active
		) {
			request.user = user;
			return true;
		}

		const response = context.switchToHttp().getResponse();
		response.set('WWW-Authenticate', 'Basic realm="Authentication required."');
		throw new UnauthorizedException(CommonErrors.UNAUTHORIZED);
	}
}
