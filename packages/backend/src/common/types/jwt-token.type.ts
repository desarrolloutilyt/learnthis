/** Type to explicitly clarify that the string is a JWT token */
export type JwtToken = string;
