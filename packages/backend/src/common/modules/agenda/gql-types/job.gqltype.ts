import Paginated from '@Common/gql-types/paginate-filter.gqltypes';
import { ObjectID } from '@Common/types/object-id.type';
import { Field, ID, ObjectType } from '@nestjs/graphql';

/**
 * Graphql type: Job
 */
@ObjectType()
export class Job {
	@Field(() => ID)
	_id: ObjectID;
	name: string;
	type: string;
	priority: number;
	nextRunAt: string;
	lastModifiedBy: string;
	lockedAt: string;
	lastRunAt: string;
	lastFinishedAt: string;
	data: Object;
}

@ObjectType()
export class JobPaginated extends Paginated<Job>(Job) {}
