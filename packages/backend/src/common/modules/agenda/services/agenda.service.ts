import { Env } from '@Common/enums/env.enum';
import { MailerService } from '@nestjs-modules/mailer';
import { Injectable, Logger } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import Agenda from 'agenda';
import { resolve } from 'path';
import { Progress } from 'progress-stream';
import { throttleTime } from 'rxjs/operators';
import { AgendaJobs } from '../enums/agenda-jobs.enum';
import {
	IEmailJobData,
	IImageProcessData,
	IUploadVideoMuxData,
} from '../interfaces/job-data.interface';
import { RxQueueWorkerPool } from './rx-queue-worker-pool.service';
import { ObjectId } from 'mongodb';
import { FileProcessService } from './file-process.service';

@Injectable()
export class AgendaService {
	private static isInit = false;
	private static agenda: Agenda;
	private static threadPoolQueue: RxQueueWorkerPool;

	/**
	 * Get Singleton instance of Agenda
	 */
	public static getInstance() {
		return AgendaService.agenda;
	}
	constructor(
		private readonly configService: ConfigService,
		private readonly mailerService: MailerService,
		private readonly fileProcesService: FileProcessService
	) {
		if (!AgendaService.isInit) {
			AgendaService.isInit = true;
			AgendaService.threadPoolQueue = new RxQueueWorkerPool(
				resolve(__dirname, './worker.js'),
				2
			);
			AgendaService.agenda = new Agenda({
				db: {
					address: this.configService.get(Env.DATABASE_URI),
					options: {
						useUnifiedTopology: true,
					},
				},
			});
			Logger.log('Agenda Initialized', 'AgendaService');
			this.defineJobs();
			AgendaService.agenda.once('ready', () => {
				AgendaService.agenda.start();
			});
			process.on('SIGTERM', this.graceful.bind(this));
			process.on('SIGINT', this.graceful.bind(this));
		}
	}

	/**
	 * Get jobs paginated from db
	 *
	 * @param skip
	 * @param limit
	 */
	async getJobs(skip: number = 0, limit: number = 10) {
		return {
			data: (await AgendaService.agenda.jobs({}, {}, limit, skip)).map(
				job => job.attrs
			),
			offset: skip,
			limit,
			total: (await AgendaService.agenda.jobs({})).length,
		};
	}

	/**
	 * Get Job by id from db
	 *
	 * @param id
	 */
	async getJob(id: string) {
		const res = await AgendaService.agenda.jobs({
			_id: new ObjectId(id),
		});
		return res[0]?.attrs;
	}

	/**
	 * Define Queue Jobs to Agenda
	 */
	private defineJobs() {
		AgendaService.agenda.define<IEmailJobData>(
			AgendaJobs.ActivationMail,
			async job => {
				await this.sendActivationMail(
					job.attrs.data.email,
					job.attrs.data.name,
					job.attrs.data.token
				);
			}
		);
		AgendaService.agenda.define<IEmailJobData>(
			AgendaJobs.ForgotPasswordMail,
			async job => {
				await this.sendForgotPasswordMail(
					job.attrs.data.email,
					job.attrs.data.name,
					job.attrs.data.token
				);
			}
		);
		AgendaService.agenda.define<IUploadVideoMuxData>(
			AgendaJobs.UploadVideoMux,
			(job, done) => {
				AgendaService.threadPoolQueue
					.run<Progress>(
						{
							fileName: job.attrs.data.fileName,
							url: job.attrs.data.url,
						},
						AgendaJobs.UploadVideoMux
					)
					.pipe(throttleTime(25000))
					.subscribe(
						data => {
							job.attrs.data.progress = data.percentage;
							job.save();
						},
						err => done(err),
						async () => {
							job.attrs.data.progress = 100;
							await job.save();
							done();
						}
					);
			}
		);
		AgendaService.agenda.define<IImageProcessData>(
			AgendaJobs.ImageProcess,
			async job => {
				await this.fileProcesService.transformImage(
					job.attrs.data.pathName,
					job.attrs.data.width,
					job.attrs.data.height
				);
			}
		);
	}

	/**
	 * Shutdown Agenda
	 */
	private async graceful() {
		await AgendaService.agenda.stop();
		await AgendaService.threadPoolQueue.destroy();
		process.exit(0);
	}

	/**
	 * Enqueue mux Image process job
	 *
	 * @param fileName
	 * @param url
	 */
	public enQueueUploadImageProcess(
		pathName: string,
		width?: number,
		height?: number
	) {
		AgendaService.agenda.now(AgendaJobs.ImageProcess, {
			pathName,
			width,
			height,
		});
	}

	/**
	 * Enqueue mux video upload job
	 *
	 * @param fileName
	 * @param url
	 */
	public enQueueUploadVideoMux(fileName: string, url: string) {
		AgendaService.agenda.now(AgendaJobs.UploadVideoMux, { fileName, url });
	}

	/**
	 * Enqueue send activation mail job
	 *
	 * @param email
	 * @param name
	 * @param token
	 */
	public enQueueActivationMail(email: string, name: string, token: string) {
		AgendaService.agenda.now(AgendaJobs.ActivationMail, { email, name, token });
	}

	/**
	 * Enqueue send forgot password mail job
	 *
	 * @param email
	 * @param name
	 * @param token
	 */
	public enQueueForgotPasswordMail(email: string, name: string, token: string) {
		AgendaService.agenda.now(AgendaJobs.ForgotPasswordMail, {
			email,
			name,
			token,
		});
	}

	/**
	 * Method to send activation email
	 *
	 * @param  {string} email
	 * @param  {string} name
	 * @param  {string} token
	 * @returns Promise
	 */
	private async sendActivationMail(
		email: string,
		name: string,
		token: string
	): Promise<boolean> {
		if (this.configService.get(Env.NODE_ENV) === 'test') return true;

		const activationLink = `${this.configService.get(
			Env.HOST_FRONT
		)}/activate/${token}`;

		await this.mailerService.sendMail({
			to: email,
			from: '"NoReply LearnThis " <noreply@learnthis.com>',
			subject: 'LearnThis - Activa tu cuenta',
			template: 'activate',
			text: `Activa tu cuenta en LearnThis a través del siguiente enlace ${activationLink}`,
			context: {
				user_firstname: name,
				confirm_link: activationLink,
			},
		});

		return true;
	}

	/**
	 * Method to send new password email
	 *
	 * @param  {string} email
	 * @param  {string} name
	 * @param  {string} token
	 * @returns Promise
	 */
	private async sendForgotPasswordMail(
		email: string,
		name: string,
		token: string
	): Promise<boolean> {
		if (this.configService.get(Env.NODE_ENV) === 'test') return true;

		const recoverLink = `${this.configService.get(
			Env.HOST_FRONT
		)}/recover/${token}`;

		await this.mailerService.sendMail({
			to: email,
			from: '"NoReply LearnThis " <noreply@learnthis.com>',
			subject: 'LearnThis - Recupera tu contraseña',
			template: 'recover',
			text: `Recupera tu contraseña en LearnThis a través del siguiente enlace ${recoverLink}`,
			context: {
				user_firstname: name,
				recover_link: recoverLink,
			},
		});

		return true;
	}
}
