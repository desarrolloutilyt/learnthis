import { createReadStream, promises } from 'fs';
import progress from 'progress-stream';
import request from 'request';
import { pipeline } from 'stream';
import { parentPort } from 'worker_threads';
import { AgendaJobs } from '../enums/agenda-jobs.enum';

export interface IPCData<T> {
	data: T;
	message?: string;
}

const uploadVideo = async (filePath: string, url: string): Promise<void> => {
	const stat = await promises.stat(filePath);

	const str = progress({
		length: stat.size,
		time: 100,
	});

	str.on('progress', progressUpload => {
		parentPort.postMessage(progressUpload);
	});

	// The URL you get back from the upload API is resumable, and the file can be uploaded using a `PUT` request (or a series of them).
	pipeline(createReadStream(filePath), str, request.put(url), async err => {
		if (err) throw err;
		else {
			await promises.unlink(filePath);
			parentPort.postMessage(null);
		}
	});
};

parentPort.on('message', async (data: IPCData<any>) => {
	if (data.message) {
		switch (data.message) {
			case AgendaJobs.UploadVideoMux:
				await uploadVideo(data.data.fileName, data.data.url);
				break;
			default:
				parentPort.postMessage(null);
		}
	}
});
