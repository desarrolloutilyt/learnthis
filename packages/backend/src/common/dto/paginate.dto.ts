import { Field, InputType, Int } from '@nestjs/graphql';

/**
 * Graphql input type: Paginated query
 */
@InputType()
export class PaginateDto {
	/** Number of elements to skip */
	@Field(() => Int, { nullable: true })
	offset!: number;

	/** Number of elements to return */
	@Field(() => Int, { nullable: true })
	limit!: number;
}
