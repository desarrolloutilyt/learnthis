import { getCurrentSession } from '@Common/decorators/transactional';
import { Env } from '@Common/enums/env.enum';
import { IPaginate } from '@Common/interfaces/mongoose.interface';
import { ObjectID } from '@Common/types/object-id.type';
import { ObjectIdTokenPayload } from '@Common/types/objectid-token-payload.type';
import { FOLDER_UPLOADS } from '@Common/utils/file-upload';
import {
	ConflictException,
	Injectable,
	Logger,
	NotFoundException,
	UnauthorizedException,
} from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { JwtService } from '@nestjs/jwt';
import { InjectModel } from '@nestjs/mongoose';
import { WorkerLoginDto } from '@Workers/dto/worker-login.dto';
import { WorkerModifyDataDto } from '@Workers/dto/worker-modify-data.dto';
import { WorkerRegisterDto } from '@Workers/dto/worker-register.dto';
import { WorkerErrors } from '@Workers/enums/worker-errors.enum';
import { WorkerModels } from '@Workers/enums/worker-models.enum';
import { WorkerRoles } from '@Workers/enums/worker-roles.enum';
import {
	IWorkerDoc,
	IWorkerModel,
} from '@Workers/interfaces/worker-document.interface';
import { WorkerLoginOutput } from '@Workers/interfaces/worker-login-output.interface';
import { compare, hash } from 'bcrypt';
import { promises as fspromises } from 'fs';
import { Model } from 'mongoose';
import { resolve } from 'path';

/**
 * Service to manage database operations on the workers table.
 */
@Injectable()
export class WorkerService {
	/**
	 * SALT for password hash
	 */
	private readonly HASH_SALT = 10;

	/**
	 * Dependency injection.
	 * @param workerModel Worker mongoose entity
	 * @param configService Config service
	 * @param jwtService JWT service
	 */
	constructor(
		@InjectModel(WorkerModels.WORKER)
		private readonly workerModel: Model<IWorkerModel>,
		private readonly configService: ConfigService,
		private readonly jwtService: JwtService
	) {}

	//#region Find

	/**
	 * Finds all existing wrorkers (with pagination).
	 * @param offset Number of elements to skip
	 * @param limit Number of elements to return
	 * @returns Workers array paginated
	 */
	async findPaginate(
		offset: number = 0,
		limit: number = 10
	): Promise<IPaginate<IWorkerDoc>> {
		return {
			data: (await this.workerModel
				.find()
				.skip(offset)
				.limit(limit)
				.exec()) as IWorkerDoc[],
			limit,
			offset,
			total: await this.workerModel.countDocuments().exec(),
		};
	}

	/**
	 * Finds a worker by id.
	 * @param workerId Worker ObjectId
	 * @returns Worker data
	 */
	findById(workerId: ObjectID): Promise<IWorkerDoc> {
		return this.workerModel
			.findById(workerId)
			.session(getCurrentSession())
			.exec() as Promise<IWorkerDoc>;
	}

	/**
	 * Finds a worker by email.
	 * @param email Worker email
	 * @returns Worker data
	 */
	findByEmail(email: string): Promise<IWorkerDoc> {
		return this.workerModel
			.findOne({
				email: email.toLowerCase(),
			})
			.exec() as Promise<IWorkerDoc>;
	}

	/**
	 * Finds workers filtered by email.
	 * @param email Worker email
	 * @returns Array with workers data
	 */
	async findByRegexEmail(email: string): Promise<IWorkerDoc[]> {
		const workers = (await this.workerModel
			.find({
				email: { $regex: new RegExp(email.toLowerCase(), 'i') },
			})
			.exec()) as IWorkerDoc[];

		return workers;
	}

	/**
	 * Finds a worker by username.
	 * @param username Worker username
	 * @returns Worker data
	 */
	findByUsername(username: string): Promise<IWorkerDoc> {
		return this.workerModel
			.findOne({
				username: { $regex: new RegExp(username, 'i') },
			})
			.exec() as Promise<IWorkerDoc>;
	}

	//#endregion

	//#region Worker

	/**
	 * Worker login
	 * @param input.email Worker email
	 * @param input.password Worker password
	 * @returns JWT token and worker data
	 */
	async login({ email, password }: WorkerLoginDto): Promise<WorkerLoginOutput> {
		const existingWorker = await this.findByEmail(email);
		if (!existingWorker)
			throw new UnauthorizedException(WorkerErrors.INVALID_LOGIN);

		const checkPass = await compare(password, existingWorker.password);
		if (!checkPass) throw new UnauthorizedException(WorkerErrors.INVALID_LOGIN);

		if (!existingWorker.active)
			throw new UnauthorizedException(WorkerErrors.NOT_ACTIVE);

		const payload: ObjectIdTokenPayload = {
			id: existingWorker._id.toString(),
		};

		const token = await this.jwtService.signAsync(payload, {
			expiresIn: this.configService.get(Env.WORKER_TOKEN_EXPIRATION),
			secret: this.configService.get(Env.WORKER_TOKEN_KEY),
		});

		return {
			token,
			user: existingWorker,
		};
	}

	/**
	 * Register a new worker.
	 * @param input.email Worker email
	 * @param input.name Worker name
	 * @param input.surname Worker surname
	 * @param input.password Worker password
	 * @param input.roles Worker roles
	 */
	async register({
		email,
		name,
		surname,
		password,
		roles,
	}: WorkerRegisterDto): Promise<void> {
		const existingWorker = await this.findByEmail(email);
		if (existingWorker) throw new ConflictException(WorkerErrors.EMAIL_IN_USE);

		const hashedPassword = await hash(password, this.HASH_SALT);

		await this.workerModel.create({
			email: email.toLowerCase(),
			name,
			surname,
			displayName: `${name} ${surname}`,
			password: hashedPassword,
			active: false,
			roles,
			teaches: [],
		});
	}

	/**
	 * Modifies worker data.
	 * @param workerId Worker ObjectId
	 * @param data New worker data
	 * @returns Worker after modification
	 */
	async modifyData(
		workerId: ObjectID,
		data: WorkerModifyDataDto
	): Promise<IWorkerDoc> {
		const workerToModify = await this.findById(workerId);
		if (!workerToModify)
			throw new NotFoundException(WorkerErrors.WORKER_NOT_FOUND);

		if (data.email) {
			const existingWorkerByEmail = await this.findByEmail(data.email);
			if (existingWorkerByEmail)
				throw new ConflictException(WorkerErrors.EMAIL_IN_USE);
		}

		if (data.username) {
			const existingWorkerByUsername = await this.findByUsername(data.username);
			if (
				existingWorkerByUsername &&
				existingWorkerByUsername._id.toString() !== workerId
			)
				throw new ConflictException(WorkerErrors.USERNAME_IN_USE);
		}

		return await workerToModify.set({ ...data }).save();
	}

	/**
	 * Permanently remove a worker.
	 * @param workerId Worker ObjectId
	 */
	async permanentDelete(workerId: ObjectID): Promise<void> {
		const existingWorker = await this.findById(workerId);
		if (!existingWorker)
			throw new NotFoundException(WorkerErrors.WORKER_NOT_FOUND);

		await existingWorker.deleteOne().exec();
	}

	//#endregion

	//#region Image

	/**
	 * Change worker profile image.
	 * @param workerId Worker ObjectId
	 * @param filename Image path
	 * @returns New image path
	 */
	async setWorkerImageById(
		workerId: ObjectID,
		filename: string
	): Promise<string> {
		const worker = await this.findById(workerId);
		return this.setWorkertImage(worker, filename);
	}

	/**
	 * Change worker profile image.
	 * @param worker Worker data
	 * @param filename Image path
	 * @returns New image path
	 */
	async setWorkertImage(worker: IWorkerDoc, filename: string): Promise<string> {
		if (!worker) throw new NotFoundException(WorkerErrors.WORKER_NOT_FOUND);
		if (worker.photo)
			await fspromises
				.unlink(
					resolve(
						FOLDER_UPLOADS,
						worker.photo.replace(
							this.configService.get(Env.SELF_DOMAIN) +
								this.configService.get(Env.UPLOADS_STATICS_PATH) +
								'/',
							''
						)
					)
				)
				.catch(error => Logger.error(error));
		worker.photo =
			this.configService.get(Env.SELF_DOMAIN) +
			this.configService.get(Env.UPLOADS_STATICS_PATH) +
			'/' +
			filename;
		await worker.save();
		return worker.photo;
	}

	//#endregion

	//#region Enable

	/**
	 * Enables a worker.
	 * @param workerId Worker ObjectId
	 */
	async enableWorker(workerId: ObjectID): Promise<void> {
		const existingWorker = await this.findById(workerId);
		if (!existingWorker)
			throw new NotFoundException(WorkerErrors.WORKER_NOT_FOUND);

		if (existingWorker.active === true)
			throw new ConflictException(WorkerErrors.WORKER_ALREADY_ENABLED);

		existingWorker.active = true;
		await existingWorker.save();
	}

	/**
	 * Disables a worker.
	 * @param workerId Worker ObjectId
	 */
	async disableWorker(workerId: ObjectID): Promise<void> {
		const existingWorker = await this.findById(workerId);
		if (!existingWorker)
			throw new NotFoundException(WorkerErrors.WORKER_NOT_FOUND);

		if (existingWorker.active === false)
			throw new ConflictException(WorkerErrors.WORKER_ALREADY_DISABLED);

		existingWorker.active = false;
		await existingWorker.save();
	}

	//#endregion

	//#region Roles

	/**
	 * Edits worker roles with new roles.
	 * @param workerId Worker ObjectId
	 * @param roles New worker roles
	 */
	async editWorkerRoles(
		workerId: ObjectID,
		roles: WorkerRoles[]
	): Promise<void> {
		const existingWorker = await this.findById(workerId);
		if (!existingWorker)
			throw new NotFoundException(WorkerErrors.WORKER_NOT_FOUND);

		if (roles[0] === WorkerRoles.ADMIN) {
			if (existingWorker.roles[0] === WorkerRoles.ADMIN)
				throw new ConflictException(WorkerErrors.WORKER_ALREADY_ADMIN);

			existingWorker.roles = [WorkerRoles.ADMIN];
		} else {
			existingWorker.roles = roles;
		}

		await existingWorker.save();
	}

	//#endregion

	//#region Courses

	/**
	 * Asign a course to a worker.
	 * @param courseId Course ObjectId
	 * @param worker Worker mongoose document
	 */
	async asignCourseToWorker(courseId: ObjectID, worker: IWorkerDoc) {
		worker.teaches.push(courseId);

		await worker.save({ session: getCurrentSession() });
	}

	/**
	 * Unassing a course from a worker.
	 * @param courseId Course ObjectId
	 * @param worker Worker mongoose document
	 */
	async unasignCourseToWorker(courseId: ObjectID, worker: IWorkerDoc) {
		worker.teaches = worker.teaches.filter(
			course => course.toString() !== courseId
		);

		await worker.save({ session: getCurrentSession() });
	}
	//#endregion
}
