import { GetGqlAuthUser } from '@Common/auth/get-user.decorator';
import { PaginateDto } from '@Common/dto/paginate.dto';
import { IPaginate } from '@Common/interfaces/mongoose.interface';
import { ObjectIDPipe } from '@Common/pipes/objectid.pipe';
import { ObjectID } from '@Common/types/object-id.type';
import { ConflictException, UseGuards } from '@nestjs/common';
import { Args, ID, Mutation, Query, Resolver } from '@nestjs/graphql';
import { AllowedWorkerRoles } from '@Workers/auth/allowed-worker-roles.decorator';
import { WorkerGqlAuthGuard } from '@Workers/auth/worker-gql-auth.guard';
import { WorkerGqlRolesGuard } from '@Workers/auth/worker-gql-roles.guard';
import { WorkerLoginDto } from '@Workers/dto/worker-login.dto';
import { WorkerModifyDataDto } from '@Workers/dto/worker-modify-data.dto';
import { WorkerRegisterDto } from '@Workers/dto/worker-register.dto';
import { WorkerErrors } from '@Workers/enums/worker-errors.enum';
import { WorkerRoles } from '@Workers/enums/worker-roles.enum';
import { WorkerLogin } from '@Workers/gql-types/worker-login.gqltype';
import { WorkerPaginated } from '@Workers/gql-types/worker-paginated.gqltype';
import { Worker } from '@Workers/gql-types/worker.gqltype';
import { IWorkerDoc } from '@Workers/interfaces/worker-document.interface';
import { WorkerLoginOutput } from '@Workers/interfaces/worker-login-output.interface';
import { WorkerLoginPipe } from '@Workers/pipes/worker-login.pipe';
import { WorkerModifyDataPipe } from '@Workers/pipes/worker-modify-data.pipe';
import { WorkerRegisterPipe } from '@Workers/pipes/worker-register.pipe';
import { WorkerService } from '@Workers/services/worker.service';

/**
 * Worker graphql queries and resolvers.
 */
@Resolver(() => Worker)
export class WorkerResolver {
	/**
	 * Dependency injection.
	 * @param workerService Worker service
	 */
	constructor(private readonly workerService: WorkerService) {}

	//#region Find

	/**
	 * Gets the current worker.
	 *
	 * - AUTH: Worker
	 * @param worker Worker data
	 * @returns Worker data
	 */
	@Query(() => Worker)
	@UseGuards(WorkerGqlAuthGuard)
	current_worker(@GetGqlAuthUser() worker: IWorkerDoc): IWorkerDoc {
		return worker;
	}

	/**
	 * Finds all existing workers (with pagination).
	 *
	 * - AUTH: Workers
	 * - ROLES: Admin
	 * @param paginate Pagination options
	 * @returns Workers array paginated
	 */
	@Query(() => WorkerPaginated)
	@AllowedWorkerRoles(WorkerRoles.ADMIN)
	@UseGuards(WorkerGqlAuthGuard, WorkerGqlRolesGuard)
	worker_find(
		@Args('paginate', { type: () => PaginateDto, nullable: true })
		paginate: PaginateDto
	): Promise<IPaginate<IWorkerDoc>> {
		return this.workerService.findPaginate(
			(paginate && paginate.offset) || 0,
			(paginate && paginate.limit) || 10
		);
	}

	/**
	 * Finds a worker by id.
	 *
	 * - AUTH: Workers
	 * - ROLES: Admin
	 * @param workerId Worker ObjectId
	 * @returns Worker data
	 */
	@Query(() => Worker)
	@AllowedWorkerRoles(WorkerRoles.ADMIN)
	@UseGuards(WorkerGqlAuthGuard, WorkerGqlRolesGuard)
	worker_find_by_id(
		@Args('workerId', { type: () => ID }, ObjectIDPipe) workerId: ObjectID
	): Promise<IWorkerDoc> {
		return this.workerService.findById(workerId);
	}

	/**
	 * Finds all workers who match the email
	 *
	 * - AUTH: Workers
	 * - ROLES: Admin
	 * @param email Worker email
	 * @returns Array with workers data
	 */
	@Query(() => [Worker])
	@AllowedWorkerRoles(WorkerRoles.ADMIN)
	@UseGuards(WorkerGqlAuthGuard, WorkerGqlRolesGuard)
	worker_find_by_regex_email(
		@Args('workerEmail') workerEmail: string
	): Promise<IWorkerDoc[]> {
		return this.workerService.findByRegexEmail(workerEmail);
	}

	//#endregion

	//#region Workers

	/**
	 * Worker login.
	 *
	 * - AUTH: Public
	 * @param input.email Worker login data
	 * @returns JWT token and worker data
	 */
	@Query(() => WorkerLogin)
	worker_login(
		@Args('input', { type: () => WorkerLoginDto }, WorkerLoginPipe)
		input: WorkerLoginDto
	): Promise<WorkerLoginOutput> {
		return this.workerService.login(input);
	}

	/**
	 * Register a new worker.
	 *
	 * - AUTH: Workers
	 * - ROLES: Admin
	 * @param input Worker data
	 * @returns True if success
	 */
	@Mutation(() => Boolean)
	@AllowedWorkerRoles(WorkerRoles.ADMIN)
	@UseGuards(WorkerGqlAuthGuard, WorkerGqlRolesGuard)
	async worker_register(
		@Args('input', { type: () => WorkerRegisterDto }, WorkerRegisterPipe)
		input: WorkerRegisterDto
	): Promise<boolean> {
		await this.workerService.register(input);
		return true;
	}

	/**
	 * Modifies worker data.
	 *
	 * - AUTH: Workers
	 * - ROLES: Admin
	 * @param workerId Worker ObjectId
	 * @param input New worker data
	 * @returns Worker after modification
	 */
	@Mutation(() => Worker)
	@AllowedWorkerRoles(WorkerRoles.ADMIN)
	@UseGuards(WorkerGqlAuthGuard, WorkerGqlRolesGuard)
	async worker_modify_data(
		@Args('workerId', { type: () => ID }, ObjectIDPipe) workerId: ObjectID,
		@Args('input', { type: () => WorkerModifyDataDto }, WorkerModifyDataPipe)
		input: WorkerModifyDataDto
	): Promise<IWorkerDoc> {
		return await this.workerService.modifyData(workerId, input);
	}

	/**
	 * Permanently remove a worker.
	 *
	 * - AUTH: Workers
	 * - ROLES: Admin
	 * @param worker Worker data
	 * @param workerId Worker ObjectId
	 * @returns True if success
	 */
	@Mutation(() => Boolean)
	@AllowedWorkerRoles(WorkerRoles.ADMIN)
	@UseGuards(WorkerGqlAuthGuard, WorkerGqlRolesGuard)
	async worker_permanent_delete(
		@GetGqlAuthUser() worker: IWorkerDoc,
		@Args('workerId', ObjectIDPipe) workerId: ObjectID
	): Promise<boolean> {
		if (worker._id.toString() === workerId)
			throw new ConflictException(WorkerErrors.CANNOT_DELETE_YOURSELF);
		await this.workerService.permanentDelete(workerId);
		return true;
	}

	//#endregion

	//#region Enable

	/**
	 * Enables a worker.
	 *
	 * - AUTH: Workers
	 * - ROLES: Admin
	 * @param workerId Worker ObjectId
	 */
	@Mutation(() => Boolean)
	@AllowedWorkerRoles(WorkerRoles.ADMIN)
	@UseGuards(WorkerGqlAuthGuard, WorkerGqlRolesGuard)
	async worker_enable(
		@Args('workerId', { type: () => ID }, ObjectIDPipe) workerId: ObjectID
	): Promise<boolean> {
		await this.workerService.enableWorker(workerId);
		return true;
	}

	/**
	 * Disables a worker (cannot disable yourself).
	 *
	 * - AUTH: Workers
	 * - ROLES: Admin
	 * @param worker Worker data
	 * @param workerId Worker ObjectId
	 */
	@Mutation(() => Boolean)
	@AllowedWorkerRoles(WorkerRoles.ADMIN)
	@UseGuards(WorkerGqlAuthGuard, WorkerGqlRolesGuard)
	async worker_disable(
		@GetGqlAuthUser() worker: IWorkerDoc,
		@Args('workerId', { type: () => ID }, ObjectIDPipe) workerId: ObjectID
	): Promise<boolean> {
		if (worker._id.toString() === workerId)
			throw new ConflictException(WorkerErrors.CANNOT_DISABLE_YOURSELF);
		await this.workerService.disableWorker(workerId);
		return true;
	}

	//#endregion

	//#region Roles

	/**
	 * Edits worker roles with new roles.
	 *
	 * - AUTH: Workers
	 * - ROLES: Admin
	 * @param workerId Worker ObjectId
	 * @param roles New worker roles
	 * @returns True if success
	 */
	@Mutation(() => Boolean)
	@AllowedWorkerRoles(WorkerRoles.ADMIN)
	@UseGuards(WorkerGqlAuthGuard, WorkerGqlRolesGuard)
	async worker_edit_roles(
		@Args('workerId', { type: () => ID }, ObjectIDPipe) workerId: ObjectID,
		@Args('roles', { type: () => [WorkerRoles] }) roles: WorkerRoles[]
	): Promise<boolean> {
		await this.workerService.editWorkerRoles(workerId, roles);
		return true;
	}

	//#endregion
}
