/** Worker error messages */
export enum WorkerErrors {
	FORMAT_NAME = 'Formato de nombre inválido',
	FORMAT_SURNAME = 'Formato de apellidos inválido',
	FORMAT_DISPLAYNAME = 'Formato de nombre para mostrar inválido',
	FORMAT_PASSWORD = 'Formato de contraseña inválido',
	FORMAT_EMAIL = 'Formato de email inválido',
	FORMAT_USERNAME = 'Formato de username inválido',
	FORMAT_BIO = 'Formato de bio inválido',
	FORMAT_PHONE = 'Formato de teléfono inválido',
	INVALID_LOGIN = 'Login incorrecto',

	NOT_ACTIVE = 'Usuario no activo',
	EMAIL_IN_USE = 'El email ya está en uso',
	USERNAME_IN_USE = 'El username ya está en uso',
	NOTHING_TO_MODIFY = 'Nada que modificar',

	WORKER_NOT_FOUND = 'No se encuentra el trabajador',
	WORKER_ALREADY_ENABLED = 'El trabajador ya está habilitado',
	WORKER_ALREADY_DISABLED = 'El trabajador ya está deshabilitado',
	CANNOT_DISABLE_YOURSELF = 'Estaba tan confuso que se desactivó a si mismo',
	CANNOT_DELETE_YOURSELF = 'Estaba tan confuso que se eliminó a si mismo',

	WORKER_ALREADY_ADMIN = 'El trabajador ya es un administrador',
	ADMIN_ONLY_ALLOW_ONE_ROLE = 'Los administradores sólo pueden tener ese rol',

	WORKER_ALREADY_COURSE = 'El trabajador ya tutoriza este curso',
	WORKER_NOT_COURSE = 'El trabajador no tutoriza este curso',
}
