import { registerEnumType } from '@nestjs/graphql';

/** Worker roles on application */
export enum WorkerRoles {
	ADMIN = 'ADMIN',
	TEACHER = 'TEACHER',
}

registerEnumType(WorkerRoles, { name: 'WorkerRoles' });
