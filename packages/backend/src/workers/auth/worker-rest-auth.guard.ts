import { ExecutionContext, Injectable } from '@nestjs/common';
import { WorkerBaseAuthGuard } from './worker-base-auth.guard';

/**
 * Only for REST requests.
 * Extracts worker from the context and sets it to req.user.
 */
@Injectable()
export class WorkerRestAuthGuard extends WorkerBaseAuthGuard {
	/**
	 * Extracts student from JWT token and adds it to req.user
	 * @param context Execution context
	 */
	getRequest(context: ExecutionContext) {
		return context.switchToHttp().getRequest();
	}
}
