import { SetMetadata } from '@nestjs/common';
import { WorkerRoles } from '@Workers/enums/worker-roles.enum';

/**
 * Decorator to set which roles can access a request.
 * @param roles Allowed roles
 */
export const AllowedWorkerRoles = (...roles: WorkerRoles[]) =>
	SetMetadata('roles', roles);
