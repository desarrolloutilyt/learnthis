import { CommonErrors } from '@Common/enums/common-errors.enum';
import {
	CanActivate,
	ExecutionContext,
	ForbiddenException,
	Injectable,
} from '@nestjs/common';
import { Reflector } from '@nestjs/core';
import { WorkerRoles } from '@Workers/enums/worker-roles.enum';
import { IWorkerDoc } from '@Workers/interfaces/worker-document.interface';

/**
 * Base guard to check if the worker is within the allowed roles for a specific request.
 *
 * - It is necessary to implement the method get user from request.
 */
@Injectable()
export abstract class WorkerBaseRoleGuard implements CanActivate {
	constructor(private readonly reflector: Reflector) {}

	/**
	 * Gets current worker from request.
	 * @param context Execution context
	 */
	abstract getWorker(context: ExecutionContext): IWorkerDoc;

	/**
	 * Determines if the user's role is within the allowed roles for the request.
	 * @param context Execution context
	 */
	canActivate(context: ExecutionContext): boolean {
		const roles = this.reflector.get<WorkerRoles[]>(
			'roles',
			context.getHandler()
		);

		if (!roles) throw new ForbiddenException(CommonErrors.UNAUTHORIZED);

		const user = this.getWorker(context);

		return this.isUserAllowed(user.roles, roles);
	}

	/**
	 * Check if the role of a user is within the array of allowed roles.
	 * @param userRoles User roles
	 * @param allowedRoles Allowed roles
	 */
	private isUserAllowed(userRoles: WorkerRoles[], allowedRoles: WorkerRoles[]) {
		for (const userRole of userRoles) {
			for (const allowedRole of allowedRoles) {
				if (userRole === allowedRole) return true;
			}
		}

		throw new ForbiddenException(CommonErrors.UNAUTHORIZED);
	}
}
