import { ExecutionContext, Injectable } from '@nestjs/common';
import { WorkerBaseRoleGuard } from './worker-base-role.guard';

/**
 * Only for REST requests.
 *
 * Determines if the user's role is within the allowed roles for the request.
 * It is mandatory to use it in combination with the AllowedWorkerRoles decorator and any Worker's auth guard.
 */
@Injectable()
export class WorkerRestRolesGuard extends WorkerBaseRoleGuard {
	/**
	 * Gets current worker from request.
	 * @param context Execution context
	 */
	getWorker(context: ExecutionContext) {
		return context.switchToHttp().getRequest().user;
	}
}
