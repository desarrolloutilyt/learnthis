import { ExecutionContext, Injectable } from '@nestjs/common';
import { GqlExecutionContext } from '@nestjs/graphql';
import { WorkerBaseRoleGuard } from './worker-base-role.guard';

/**
 * Only for GRAPHQL requests.
 *
 * Determines if the user's role is within the allowed roles for that method.
 * It is mandatory to use it in combination with the AllowedWorkerRoles decorator and any Worker's auth guard.
 */
@Injectable()
export class WorkerGqlRolesGuard extends WorkerBaseRoleGuard {
	/**
	 * Gets current worker from request.
	 * @param context Execution context
	 */
	getWorker(context: ExecutionContext) {
		return GqlExecutionContext.create(context).getContext().req.user;
	}
}
