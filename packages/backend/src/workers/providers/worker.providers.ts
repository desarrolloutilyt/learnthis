import { Provider } from '@nestjs/common';
import { WorkerResolver } from '@Workers/resolvers/worker.resolver';
import { WorkerService } from '@Workers/services/worker.service';

/**
 * Worker providers
 */
export const workerProviders: Provider[] = [WorkerResolver, WorkerService];
