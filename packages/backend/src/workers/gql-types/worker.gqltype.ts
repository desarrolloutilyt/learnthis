import { ObjectID } from '@Common/types/object-id.type';
import { Field, ID, ObjectType } from '@nestjs/graphql';
import { WorkerRoles } from '@Workers/enums/worker-roles.enum';

/**
 * Graphql type: Worker
 */
@ObjectType()
export class Worker {
	/** Mongo ObjectId */
	@Field(() => ID)
	_id: ObjectID;
	/** Worker's email */
	email: string;
	/** Worker's name */
	name: string;
	/** Worker's surname */
	surname: string;
	/** Worker's full name to display */
	displayName: string;
	/** True if worker is activated */
	active: boolean;
	/** Worker's contact phone number */
	phone?: string;
	/** Worker's username */
	username?: string;
	/** Worker's profile picture URL */
	photo?: string;
	/** Worker's biography */
	bio?: string;
	/** Worker's roles on the platform */
	roles: WorkerRoles[];
	/** Courses tutored by worker */
	@Field(() => [ID])
	teaches: ObjectID[];
}
