import { CourseModels } from '@Courses/enums/course-models.enum';
import { Course } from '@Courses/schemas/course.schema';
import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { WorkerRoles } from '@Workers/enums/worker-roles.enum';
import { Schema as MongooseSchema } from 'mongoose';

/**
 * Mongoose worker's schema definition
 */
@Schema()
export class Worker {
	/** Worker's email */
	@Prop({ type: String, required: true, unique: true })
	email: string;

	/** Worker's name */
	@Prop({ type: String, required: true })
	name: string;

	/** Worker's surname */
	@Prop({ type: String, required: true })
	surname: string;

	/** Worker's full name to display */
	@Prop({ type: String })
	displayName: string;

	/** Worker's password */
	@Prop({ type: String, required: true })
	password: string;

	/** True if worker account active */
	@Prop({ type: Boolean, required: true, default: false })
	active: boolean;

	/** Worker's phone */
	@Prop({ type: String })
	phone: string;

	/** Worker's usename*/
	@Prop({ type: String })
	username: string;

	/** Worker's profile picture URL*/
	@Prop({ type: String })
	photo: string;

	/** Worker's biography*/
	@Prop({ type: String })
	bio: string;

	/** Worker's roles on the platform */
	@Prop({ type: [{ type: String, enum: Object.values(WorkerRoles) }] })
	roles: WorkerRoles[];

	/** Courses tutored by worker */
	@Prop({
		type: [{ type: MongooseSchema.Types.ObjectId, ref: CourseModels.COURSE }],
	})
	teaches: Course[];
}

/**
 * Mongoose worker's schema object
 */
export const WorkerSchema = SchemaFactory.createForClass(Worker);

WorkerSchema.index({ email: 1 });
