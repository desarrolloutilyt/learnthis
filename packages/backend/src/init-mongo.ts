import { Env } from '@Common/enums/env.enum';
import { AgendaService } from '@Common/modules/agenda/services/agenda.service';
import { FileProcessService } from '@Common/modules/agenda/services/file-process.service';
import { MuxService } from '@Common/modules/agenda/services/mux.service';
import { CourseModels } from '@Courses/enums/course-models.enum';
import { LessonTypes } from '@Courses/enums/lesson-types.enum';
import { ICourseDoc } from '@Courses/interfaces/course-document.interface';
import {
	ILessonDoc,
	IVideoLessonDoc,
} from '@Courses/interfaces/lesson-document.interface';
import { MailerService } from '@nestjs-modules/mailer';
import { EjsAdapter } from '@nestjs-modules/mailer/dist/adapters/ejs.adapter';
import { ConfigService } from '@nestjs/config';
import { StudentModels } from '@Students/enums/student-models.enum';
import {
	IStudent,
	IStudentDoc,
} from '@Students/interfaces/student-document.interface';
import { WorkerModels } from '@Workers/enums/worker-models.enum';
import { WorkerRoles } from '@Workers/enums/worker-roles.enum';
import {
	IWorker,
	IWorkerDoc,
} from '@Workers/interfaces/worker-document.interface';
import { WorkerSchema } from '@Workers/schemas/worker.schema';
import { config } from 'dotenv';
import { connect, model } from 'mongoose';
import { join } from 'path';
import { CourseSchema } from './courses/schemas/course.schema';
import {
	InfoLessonSchema,
	LessonSchema,
	VideoLessonSchema,
} from './courses/schemas/lesson.schema';
import { StudentSchema } from './students/schemas/students.schema';

config();

//#region DATA

const studentData: IStudent = {
	email: 'testing2@test.com',
	name: 'bullchit',
	surname: 'Tanenbaum',
	// test1234
	password: '$2b$12$l9ElSZrSp02bPP.XlsuSQeatkx1AQ7SefCW5VGXFf/vsoIGhPvaPm',
	active: true,
	socialAccounts: [],
	cart: [],
	coursesEnrolled: [],
	orders: [],
};

const coursesData = [
	{
		url: 'typescript',
		title: 'Typescript con mucha clase',
		description:
			'¿Quieres tipos? Toma tipos. Te enseñamos a crear interfaces, clases y tipos, y cómo utilizarlas correctamente mediante any.',
		image: `${process.env.SELF_DOMAIN}/uploads/typescript.png`,
		background: `${process.env.SELF_DOMAIN}/uploads/typescriptBG.png`,
		price: 49,
		sections: [
			{
				title: 'Introducción a Typescript',
				description:
					'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras volutpat leo elit, et vestibulum nulla consectetur molestie. Donec sit amet consectetur ante. Fusce vitae libero quam. Praesent massa quam, dignissim non justo at, condimentum posuere ex. Cras nec neque sapien.',
				lessons: [
					{
						kind: LessonTypes.VIDEO,
						title: '¿Qué es typescript?',
						description:
							'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras volutpat leo elit, et vestibulum nulla consectetur molestie. Donec sit amet consectetur ante. Fusce vitae libero quam. Praesent massa quam, dignissim non justo at, condimentum posuere ex. Cras nec neque sapien.',
						duration: 230000,
					},
					{
						kind: LessonTypes.VIDEO,
						title: 'Instalar Typescript',
						description:
							'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras volutpat leo elit, et vestibulum nulla consectetur molestie. Donec sit amet consectetur ante. Fusce vitae libero quam. Praesent massa quam, dignissim non justo at, condimentum posuere ex. Cras nec neque sapien.',
						duration: 230000,
					},
					{
						kind: LessonTypes.INFO,
						title: 'Comandos TSC',
						description:
							'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras volutpat leo elit, et vestibulum nulla consectetur molestie. Donec sit amet consectetur ante. Fusce vitae libero quam. Praesent massa quam, dignissim non justo at, condimentum posuere ex. Cras nec neque sapien.',
						content: `
							# Lección de info
							Esto es un ejemplo de lección de info
						`,
					},
				],
			},
			{
				title: 'Variables y tipos de datos',
				description:
					'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras volutpat leo elit, et vestibulum nulla consectetur molestie. Donec sit amet consectetur ante. Fusce vitae libero quam. Praesent massa quam, dignissim non justo at, condimentum posuere ex. Cras nec neque sapien.',
				lessons: [
					{
						kind: LessonTypes.VIDEO,
						title: 'Variables',
						description:
							'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras volutpat leo elit, et vestibulum nulla consectetur molestie. Donec sit amet consectetur ante. Fusce vitae libero quam. Praesent massa quam, dignissim non justo at, condimentum posuere ex. Cras nec neque sapien.',
						duration: 230000,
					},
				],
			},
			{
				title: 'Objetos y arrays',
				description:
					'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras volutpat leo elit, et vestibulum nulla consectetur molestie. Donec sit amet consectetur ante. Fusce vitae libero quam. Praesent massa quam, dignissim non justo at, condimentum posuere ex. Cras nec neque sapien.',
				lessons: [
					{
						kind: LessonTypes.VIDEO,
						title: 'Objetos',
						description:
							'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras volutpat leo elit, et vestibulum nulla consectetur molestie. Donec sit amet consectetur ante. Fusce vitae libero quam. Praesent massa quam, dignissim non justo at, condimentum posuere ex. Cras nec neque sapien.',
						duration: 230000,
					},
					{
						kind: LessonTypes.VIDEO,
						title: 'Arrays',
						description:
							'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras volutpat leo elit, et vestibulum nulla consectetur molestie. Donec sit amet consectetur ante. Fusce vitae libero quam. Praesent massa quam, dignissim non justo at, condimentum posuere ex. Cras nec neque sapien.',
						duration: 230000,
					},
				],
			},
			{
				title: 'Funciones',
				description:
					'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras volutpat leo elit, et vestibulum nulla consectetur molestie. Donec sit amet consectetur ante. Fusce vitae libero quam. Praesent massa quam, dignissim non justo at, condimentum posuere ex. Cras nec neque sapien.',
				lessons: [
					{
						kind: LessonTypes.VIDEO,
						title: 'Funciones',
						description:
							'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras volutpat leo elit, et vestibulum nulla consectetur molestie. Donec sit amet consectetur ante. Fusce vitae libero quam. Praesent massa quam, dignissim non justo at, condimentum posuere ex. Cras nec neque sapien.',
						duration: 230000,
					},
				],
			},
		],
	},
	{
		url: 'javascript',
		title: 'Javascript for dummies',
		description:
			'Un lenguaje que no tiene tipos, no es serio, no es ario, hace cosas raras, pero funciona mucho mejor que PHP.',
		image: `${process.env.SELF_DOMAIN}/uploads/javascript.png`,
		price: 9,
		compareAtPrice: 999,
		sections: [
			{
				title: 'Introducción a Javascript',
				description:
					'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras volutpat leo elit, et vestibulum nulla consectetur molestie. Donec sit amet consectetur ante. Fusce vitae libero quam. Praesent massa quam, dignissim non justo at, condimentum posuere ex. Cras nec neque sapien.',
				lessons: [
					{
						kind: LessonTypes.VIDEO,
						title: '¿Qué es Javascript?',
						description:
							'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras volutpat leo elit, et vestibulum nulla consectetur molestie. Donec sit amet consectetur ante. Fusce vitae libero quam. Praesent massa quam, dignissim non justo at, condimentum posuere ex. Cras nec neque sapien.',
						duration: 230000,
					},
				],
			},
		],
	},
	{
		url: 'mongodb',
		title: 'MongoDB para gente bien relacionada',
		description:
			'El populismo explicado como nunca antes. Te enseñamos a fortalecer tus relaciones y crear tablas estructuradas, para que después puedas guardar dentro cualquier mierda.',
		image: `${process.env.SELF_DOMAIN}/uploads/mongodb.png`,
		price: 49,
		compareAtPrice: 55,
		sections: [
			{
				title: 'Introducción a MongoDB',
				description:
					'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras volutpat leo elit, et vestibulum nulla consectetur molestie. Donec sit amet consectetur ante. Fusce vitae libero quam. Praesent massa quam, dignissim non justo at, condimentum posuere ex. Cras nec neque sapien.',
				lessons: [
					{
						kind: LessonTypes.VIDEO,
						title: '¿Qué es MongoDB?',
						description:
							'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras volutpat leo elit, et vestibulum nulla consectetur molestie. Donec sit amet consectetur ante. Fusce vitae libero quam. Praesent massa quam, dignissim non justo at, condimentum posuere ex. Cras nec neque sapien.',
						duration: 230000,
					},
				],
			},
		],
	},
	{
		url: 'graphql',
		title: 'GraphQl machine',
		description:
			'Mira que grafos más guapos, encima rositas. Cómo no va a ser mejor que los cutres de REST',
		image: `${process.env.SELF_DOMAIN}/uploads/graphql.png`,
		price: 39,
		sections: [
			{
				title: 'Introducción a Graphql',
				description:
					'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras volutpat leo elit, et vestibulum nulla consectetur molestie. Donec sit amet consectetur ante. Fusce vitae libero quam. Praesent massa quam, dignissim non justo at, condimentum posuere ex. Cras nec neque sapien.',
				lessons: [
					{
						kind: LessonTypes.VIDEO,
						title: '¿Qué es Graphql?',
						description:
							'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras volutpat leo elit, et vestibulum nulla consectetur molestie. Donec sit amet consectetur ante. Fusce vitae libero quam. Praesent massa quam, dignissim non justo at, condimentum posuere ex. Cras nec neque sapien.',
						duration: 230000,
					},
				],
			},
		],
	},
];

const workerData: IWorker = {
	email: 'admin@test.com',
	name: 'Admin',
	surname: 'Admin',
	displayName: 'El root',
	roles: [WorkerRoles.ADMIN],
	// test1234
	password: '$2b$12$l9ElSZrSp02bPP.XlsuSQeatkx1AQ7SefCW5VGXFf/vsoIGhPvaPm',
	active: true,
	teaches: [],
};

//#endregion

const sleep = (duration: number) =>
	new Promise((resolve, reject) =>
		setTimeout(() => {
			resolve(0);
		}, duration)
	);

const uploadVideo = async (): Promise<string> => {
	const configService = new ConfigService();
	const muxService = new MuxService(
		configService,
		new AgendaService(
			configService,
			new MailerService({
				transport: {
					host: configService.get(Env.MAIL_HOST),
					port: Number(configService.get(Env.MAIL_PORT)),
					ignoreTLS: Boolean(configService.get(Env.MAIL_TLS)),
				},
				defaults: {
					from: '"NoReply LearnThis " <noreply@learnthis.com>',
				},
				template: {
					dir: join(__dirname, '../templates'),
					adapter: new EjsAdapter(),
					options: {
						strict: true,
					},
				},
			}),
			new FileProcessService()
		)
	);

	let assetsList = await muxService.listAssets(1);

	if (!assetsList.length) {
		await muxService.uploadPublicVideo('../mux-video-intro.mp4');

		do {
			assetsList = await muxService.listAssets(1);
			await sleep(2000);
		} while (!assetsList.length);
	}

	return assetsList[0].playback_ids[0].id;
};

export const initDB = async () => {
	console.log('\nCreating DB example data\n');

	const connection = await connect(process.env.DATABASE_URI, {
		useNewUrlParser: true,
		useUnifiedTopology: true,
		useFindAndModify: false,
		useCreateIndex: true,
	});

	const studentModel = model(StudentModels.STUDENT, StudentSchema);
	const courseModel = model(CourseModels.COURSE, CourseSchema);
	const lessonModel = model(CourseModels.LESSON, LessonSchema);
	const infoLessonModel = lessonModel.discriminator(
		CourseModels.LESSON_INFO,
		InfoLessonSchema
	);
	const videoLessonModel = lessonModel.discriminator(
		CourseModels.LESSON_VIDEO,
		VideoLessonSchema
	);
	const workerModel = model(WorkerModels.WORKER, WorkerSchema);

	//Video upload
	const playbackUrl = await uploadVideo();

	const student = await studentModel
		.findOne({ email: studentData.email })
		.exec();

	if (!student) {
		console.log('Generating example students...');
		const newStudent = new studentModel(studentData) as IStudentDoc;
		await newStudent.save();
	}

	const worker = await workerModel.findOne({ email: workerData.email }).exec();

	if (!worker) {
		console.log('Generating example workers...');
		const newWorker = new workerModel(workerData) as IWorkerDoc;
		await newWorker.save();
	}

	const courses = (await courseModel.find().exec()) as ICourseDoc<
		ILessonDoc | undefined
	>[];

	if (!courses || !courses.length) {
		console.log('Generating example courses...');

		for (const courseData of coursesData) {
			console.log('  ->', courseData.url);
			const newCourse = new courseModel({
				url: courseData.url,
				title: courseData.title,
				description: courseData.description,
				image: courseData.image,
				background: courseData.background,
				price: courseData.price,
				compareAtPrice: courseData.compareAtPrice,
				visibility: true,
				studentsEnrolled: [],
				sections: [],
			}) as ICourseDoc;

			if (courseData.sections) {
				for (const sectionData of courseData.sections) {
					console.log('    ->', sectionData.title);
					const lessonIds = [];

					if (sectionData.lessons) {
						for (const lessonData of sectionData.lessons) {
							console.log('      ->', lessonData.title);

							if (lessonData.kind === LessonTypes.VIDEO) {
								delete lessonData.kind;
								const newVideoLesson = new videoLessonModel({
									...lessonData,
									videoSrc: `${process.env.MUX_URL}${playbackUrl}`,
									course: newCourse._id,
									visibility: true,
								});
								await newVideoLesson.save();

								lessonIds.push(newVideoLesson.id);
							} else if (lessonData.kind === LessonTypes.INFO) {
								delete lessonData.kind;
								const newInfoLesson = new infoLessonModel({
									...lessonData,
									course: newCourse._id,
									visibility: true,
								});
								await newInfoLesson.save();
								lessonIds.push(newInfoLesson.id);
							}
						}
					}
					newCourse.sections.push({
						title: sectionData.title,
						description: sectionData.description,
						visibility: true,
						lessons: lessonIds,
					});
				}

				await newCourse.save();
			}
		}

		console.log('\nDONE\n');
	}

	//Update the url of the video, since the video in test is deleted after 24 hours
	for (const courseData of courses) {
		if (courseData.sections) {
			for (const sectionData of courseData.sections) {
				if (sectionData.lessons) {
					for (const lessonId of sectionData.lessons) {
						const lessonData = (await lessonModel
							.findById(lessonId)
							.exec()) as IVideoLessonDoc;
						if (lessonData.kind === LessonTypes.VIDEO) {
							lessonData.videoSrc = `${process.env.MUX_URL}${playbackUrl}`;
							await lessonData.save();
						}
					}
				}
			}
		}
	}

	await connection.disconnect();
};
