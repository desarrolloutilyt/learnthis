import { CourseModels } from '@Courses/enums/course-models.enum';
import { Course } from '@Courses/schemas/course.schema';
import { Prop, raw, Schema, SchemaFactory } from '@nestjs/mongoose';
import { OrderStatus } from '@Orders/enums/order-status.enum';
import { IOrderLine } from '@Orders/interfaces/order-document.interface';
import { StudentModels } from '@Students/enums/student-models.enum';
import { Student } from '@Students/schemas/students.schema';
import { Schema as MongooseSchema } from 'mongoose';

type OrderLinesProps = 'course' | 'priceWithoutTax' | 'priceWithTax';

interface IOrderLinesProps {
	course: Course;
	priceWithoutTax: number;
	priceWithTax: number;
}
@Schema({ timestamps: true })
export class Order {
	@Prop({ type: MongooseSchema.Types.ObjectId, ref: StudentModels.STUDENT })
	student: Student;

	@Prop(
		raw([
			{
				course: {
					type: MongooseSchema.Types.ObjectId,
					ref: CourseModels.COURSE,
					required: true,
				},
				priceWithoutTax: { type: Number, required: true },
				priceWithTax: { type: Number, required: true },
			},
		])
	)
	orderLines: Record<OrderLinesProps, IOrderLinesProps>[];

	@Prop({ type: String, enum: Object.values(OrderStatus), required: true })
	status: OrderStatus;
}

export const OrderSchema = SchemaFactory.createForClass(Order);

OrderSchema.virtual('totalWithTax').get(function () {
	return this.orderLines
		.map((i: IOrderLine) => i.priceWithTax)
		.reduce((a: number, b: number) => a + b);
});

OrderSchema.virtual('totalWithoutTax').get(function () {
	return this.orderLines
		.map((i: IOrderLine) => i.priceWithoutTax)
		.reduce((a: number, b: number) => a + b);
});
