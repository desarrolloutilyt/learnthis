import { Field, Float, ObjectType } from '@nestjs/graphql';

/**
 * Graphql type: Course with all its data (for admin purposes).
 */ @ObjectType()
export class Course {
	_id: string;
	url: string;
	title: string;
	description: string;
	image?: string;
	background?: string;
	@Field(type => Float)
	price: number;
	@Field(type => Float)
	compareAtPrice?: number;
	visibility: boolean;
}
