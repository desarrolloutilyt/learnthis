import Paginated from '@Common/gql-types/paginate-filter.gqltypes';
import { ObjectType } from '@nestjs/graphql';
import { CourseWithSections } from './course-with-sections.gqltype';

/**
 * Graphql type: Paginated list of courses
 */
@ObjectType()
export class CoursePaginated extends Paginated<CourseWithSections>(
	CourseWithSections
) {}
