import {
	getCurrentSession,
	WithTransaction,
} from '@Common/decorators/transactional';
import { ObjectID } from '@Common/types/object-id.type';
import { LessonCreateDto } from '@Courses/dto/lesson-create.dto';
import { LessonResourcesDto } from '@Courses/dto/lesson-resources.dto';
import { CourseErrors } from '@Courses/enums/course-errors.enum';
import { CourseModels } from '@Courses/enums/course-models.enum';
import { LessonErrors } from '@Courses/enums/lesson-errors.enum';
import { LessonTypes } from '@Courses/enums/lesson-types.enum';
import {
	ILesson,
	ILessonDoc,
	IVideoLessonDoc,
	TInfoLessonModel,
	TLessonModel,
	TVideoLessonModel,
} from '@Courses/interfaces/lesson-document.interface';
import {
	ConflictException,
	forwardRef,
	Inject,
	Injectable,
	NotFoundException,
} from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { CourseService } from './course.service';

/**
 * Service to manage database operations on the lesson table.
 */
@Injectable()
export class LessonService {
	/**
	 * Dependency injection.
	 * @param lessonModel Lesson mongoose entity
	 * @param videoLessonModel VideoLesson mongoose entity
	 * @param infoLessonModel InfoLesson mongoose entity
	 * @param courseService Course service
	 */
	constructor(
		@InjectModel(CourseModels.LESSON)
		private readonly lessonModel: Model<TLessonModel>,
		@InjectModel(CourseModels.LESSON_VIDEO)
		private readonly videoLessonModel: Model<TVideoLessonModel>,
		@InjectModel(CourseModels.LESSON_INFO)
		private readonly infoLessonModel: Model<TInfoLessonModel>,
		@Inject(forwardRef(() => CourseService))
		private readonly courseService: CourseService
	) {}

	//#region Public

	/**
	 * Finds a lesson by id.
	 * Only returns lessons whose visibility is public.
	 * @param lessonId Lesson ObjectId
	 * @returns Lesson data
	 */
	publicFindById(lessonId: ObjectID): Promise<ILessonDoc | undefined> {
		return this.lessonModel
			.findOne({ _id: lessonId, visibility: true })
			.exec() as Promise<ILessonDoc>;
	}

	//#endregion

	//#region Find

	/**
	 * Finds a lesson by id.
	 * @param lessonId Lesson ObjectId
	 * @returns Lesson data
	 */
	findById(lessonId: ObjectID): Promise<ILessonDoc | undefined> {
		return this.lessonModel.findById(lessonId).exec() as Promise<ILessonDoc>;
	}

	//#endregion

	//#region Lesson

	/**
	 * Creates a new video or info lesson, and adds it to a course section.
	 * @param courseId Course ObjectId
	 * @param sectionIndex Section index on course sections array
	 * @param input.title Lesson title
	 * @param input.description Lesson description
	 * @param input.type Lesson type
	 */
	@WithTransaction
	async createLesson(
		courseId: ObjectID,
		sectionIndex: number,
		{ title, description, type }: LessonCreateDto
	): Promise<void> {
		const session = getCurrentSession();
		const existingCourse = await this.courseService.findById(courseId);

		if (!existingCourse)
			throw new NotFoundException(CourseErrors.COURSE_NOT_FOUND);

		if (
			!existingCourse.sections ||
			existingCourse.sections.length - 1 < sectionIndex
		)
			throw new NotFoundException(CourseErrors.SECTION_NOT_FOUND);

		const newLessonBase: ILesson = {
			title,
			description,
			course: courseId,
			comments: [],
			visibility: false,
			resources: {
				links: [],
				files: [],
			},
		};

		const newLesson =
			type === LessonTypes.INFO
				? await this.infoLessonModel.create(
						[{ ...newLessonBase, content: '' }],
						{ session }
				  )
				: await this.videoLessonModel.create(
						[
							{
								...newLessonBase,
								videoSrc: '',
								duration: 0,
							},
						],
						{ session }
				  );

		await this.courseService.addLessonToSection(
			existingCourse,
			sectionIndex,
			newLesson[0]._id
		);
	}

	/**
	 * Updates lesson resources.
	 * @param lessonId Course ObjectId
	 * @param input.files Lesson resource files
	 * @param input.links Lesson resource links
	 */
	async updateResources(
		lessonId: ObjectID,
		{ files, links }: LessonResourcesDto
	): Promise<void> {
		const existingLesson = await this.lessonModel.findById(lessonId).exec();

		if (!existingLesson)
			throw new NotFoundException(CourseErrors.LESSON_NOT_FOUND);

		existingLesson.resources.files = files;
		existingLesson.resources.links = links;

		await existingLesson.save();
	}

	/**
	 * Deletes a lesson by id.
	 * @param lessonId Lesson ObjectId
	 */
	async deleteById(lessonId: ObjectID): Promise<void> {
		const existingLesson = await this.findById(lessonId);

		if (!existingLesson)
			throw new NotFoundException(LessonErrors.LESSON_NOT_FOUND);

		await existingLesson.remove().exec();
	}

	//#endregion

	//#region Publish

	/**
	 * Publishes a lesson.
	 * @param lessonId Lesson ObjectId
	 */
	async publish(lessonId: ObjectID): Promise<void> {
		const existingLesson = await this.findById(lessonId);

		if (!existingLesson)
			throw new NotFoundException(LessonErrors.LESSON_NOT_FOUND);
		if (existingLesson.visibility === true)
			throw new ConflictException(LessonErrors.LESSON_ALREADY_PUBLISHED);
		if (
			existingLesson.kind === CourseModels.LESSON_VIDEO &&
			!(existingLesson as IVideoLessonDoc).videoSrc
		)
			throw new NotFoundException(LessonErrors.VIDEOLESSON_NOT_VIDEO);

		existingLesson.visibility = true;

		await existingLesson.save();
	}

	/**
	 * Unpublishes a lesson.
	 * @param lessonId Lesson ObjectId
	 */
	async unpublish(lessonId: ObjectID): Promise<void> {
		const existingLesson = await this.findById(lessonId);

		if (!existingLesson)
			throw new NotFoundException(LessonErrors.LESSON_NOT_FOUND);
		if (existingLesson.visibility === false)
			throw new ConflictException(LessonErrors.LESSON_ALREADY_PRIVATE);

		existingLesson.visibility = false;

		await existingLesson.save();
	}

	//#endregion
}
