import {
	getCurrentSession,
	WithTransaction,
} from '@Common/decorators/transactional';
import { Env } from '@Common/enums/env.enum';
import { IPaginate } from '@Common/interfaces/mongoose.interface';
import { ObjectID } from '@Common/types/object-id.type';
import { FOLDER_UPLOADS } from '@Common/utils/file-upload';
import { CourseCreateDto } from '@Courses/dto/course-create.dto';
import { CourseModifyDto } from '@Courses/dto/course-modify.dto';
import { SectionCreateDto } from '@Courses/dto/section-create.dto';
import { SectionModifyDto } from '@Courses/dto/section-modify.dto';
import { CourseErrors } from '@Courses/enums/course-errors.enum';
import { CourseModels } from '@Courses/enums/course-models.enum';
import { ILessonDoc } from '@Courses/interfaces/lesson-document.interface';
import {
	BadRequestException,
	ConflictException,
	Injectable,
	Logger,
	NotFoundException,
} from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { InjectModel } from '@nestjs/mongoose';
import { WorkerErrors } from '@Workers/enums/worker-errors.enum';
import { WorkerService } from '@Workers/services/worker.service';
import { promises as fspromises } from 'fs';
import { Model } from 'mongoose';
import { resolve } from 'path';
import {
	ICourseDoc,
	ISectionDoc,
	TCourseModel,
} from '../interfaces/course-document.interface';

/**
 * Service to manage database operations on the course table.
 */
@Injectable()
export class CourseService {
	/**
	 * Dependency injection.
	 * @param courseModel Course mongoose entity
	 * @param configService Config service
	 */
	constructor(
		@InjectModel(CourseModels.COURSE)
		private readonly courseModel: Model<TCourseModel>,
		private readonly configService: ConfigService,
		private readonly workerService: WorkerService
	) {}

	//#region Public find

	/**
	 * Finds all existing courses.
	 * Only returns courses whose visibility is public.
	 * @returns Courses array
	 */
	publicFind(): Promise<ICourseDoc[]> {
		return this.courseModel.find({ visibility: true }).exec() as Promise<
			ICourseDoc[]
		>;
	}

	/**
	 * Finds a course by id.
	 * Only returns courses whose visibility is public.
	 * @param courseId Course ObjectId
	 * @returns Course data
	 */
	publicFindById(courseId: ObjectID): Promise<ICourseDoc | undefined> {
		return this.courseModel
			.findOne({ _id: courseId, visibility: true })
			.exec() as Promise<ICourseDoc>;
	}

	/**
	 * Finds a course by url and populates all the lessons.
	 * Only returns courses whose visibility is public.
	 * @param courseUrl Course url
	 * @returns Course with lessons
	 */
	publicFindByUrlAndPopulate(
		courseUrl: string
	): Promise<ICourseDoc<ILessonDoc | undefined>> {
		return this.courseModel
			.findOne({ url: courseUrl, visibility: true })
			.populate('sections.lessons')
			.exec() as Promise<ICourseDoc<ILessonDoc>>;
	}

	/**
	 * Finds some courses by an array of urls.
	 * Only returns courses whose visibility is public.
	 * @param coursesUrls Course urls array
	 * @returns Courses array
	 */
	publicFindByUrlArray(coursesUrls: string[]): Promise<ICourseDoc[]> {
		return this.courseModel
			.find({ _id: { $in: [...coursesUrls] }, visibility: true })
			.exec() as Promise<ICourseDoc[]>;
	}

	//#endregion

	//#region Find

	/**
	 * Finds all existing courses (with pagination).
	 * @param offset Number of elements to skip
	 * @param limit Number of elements to return
	 * @returns Courses array paginated
	 */
	async findPaginate(
		offset: number = 0,
		limit: number = 10
	): Promise<IPaginate<ICourseDoc>> {
		return {
			data: (await this.courseModel
				.find()
				.skip(offset)
				.limit(limit)
				.exec()) as ICourseDoc[],
			limit,
			offset,
			total: await this.courseModel.countDocuments().exec(),
		};
	}

	/**
	 *	Finds a course by url.
	 * @param courseUrl Course url
	 * @returns Course data
	 */
	findByUrl(courseUrl: string): Promise<ICourseDoc | undefined> {
		return this.courseModel
			.findOne({ url: courseUrl })
			.exec() as Promise<ICourseDoc>;
	}

	/**
	 * Finds a course by id.
	 * @param courseId Course ObjectId
	 * @returns Course data
	 */
	findById(courseId: ObjectID): Promise<ICourseDoc | undefined> {
		return this.courseModel
			.findById(courseId)
			.session(getCurrentSession())
			.exec() as Promise<ICourseDoc>;
	}

	/**
	 * Finds some courses by an array of ids.
	 * @param courseIds Course ObjectIds array
	 * @returns Courses array
	 */
	findByIdArray(courseIds: ObjectID[]): Promise<ICourseDoc[]> {
		return this.courseModel.find().where('_id').in(courseIds).exec() as Promise<
			ICourseDoc[]
		>;
	}

	/**
	 * Finds a course by id and populates all the lessons.
	 * @param courseId Course ObjectId
	 * @returns Course with lessons
	 */
	findByIdAndPopulate(
		courseId: ObjectID
	): Promise<ICourseDoc<ILessonDoc> | undefined> {
		return this.courseModel
			.findById(courseId)
			.populate('sections.lessons')
			.exec() as Promise<ICourseDoc<ILessonDoc>>;
	}

	/**
	 * Finds a course by url and populates all the lessons.
	 * @param courseUrl Course url
	 * @returns Course with lessons
	 */
	findByUrlAndPopulate(
		courseUrl: string
	): Promise<ICourseDoc<ILessonDoc> | undefined> {
		return this.courseModel
			.findOne({ url: courseUrl })
			.populate('sections.lessons')
			.exec() as Promise<ICourseDoc<ILessonDoc>>;
	}

	/**
	 * Populates lessons from a course document
	 * @param course Course mongoose document
	 * @returns Course with lessons
	 */
	populateLessons(course: ICourseDoc): Promise<ICourseDoc<ILessonDoc>> {
		return (course
			.populate('sections.lessons')
			.execPopulate() as Partial<ICourseDoc>) as Promise<
			ICourseDoc<ILessonDoc>
		>;
	}

	//#endregion

	//#region Course

	/**
	 * Creates a new course.
	 * @param courseData Course creation data
	 * @returns New course ObjectId
	 */
	async create(courseData: CourseCreateDto): Promise<ObjectID> {
		const { url } = courseData;

		const existingCourse = await this.findByUrl(url);

		if (existingCourse)
			throw new ConflictException(CourseErrors.COURSE_ALREADY_EXISTS);

		const course = await this.courseModel.create({
			...courseData,
			visibility: false,
			studentsEnrolled: [],
			sections: [],
			tutors: [],
		});

		return course._id;
	}

	/**
	 * Modifies an axisting course data.
	 * @param courseId Course ObjectId
	 * @param input.title Course new title
	 * @param input.description Course new price
	 * @param input.price Course new price
	 * @param input.compareAtPriceCourse new compare at price
	 * @param input.url Course new url
	 */
	async modifyCourse(
		courseId: ObjectID,
		{ title, description, price, compareAtPrice, url }: CourseModifyDto
	): Promise<void> {
		const existingCourse = await this.findById(courseId);

		if (!existingCourse)
			throw new NotFoundException(CourseErrors.COURSE_NOT_FOUND);

		if (title) existingCourse.title = title;
		if (description) existingCourse.description = description;
		if (price) existingCourse.price = price;
		if (compareAtPrice) {
			const originalPrice = price || existingCourse.price;

			if (compareAtPrice <= originalPrice)
				throw new BadRequestException(
					CourseErrors.COURSE_COMPARE_AT_LESS_THAN_PRICE
				);

			existingCourse.compareAtPrice = compareAtPrice;
		}

		if (url) {
			const existingCourseByUrl = this.findByUrl(url);

			if (existingCourseByUrl)
				throw new ConflictException(CourseErrors.COURSE_ALREADY_EXISTS);

			existingCourse.url = url;
		}

		await existingCourse.save();
	}

	/**
	 * Sorts sections in a course.
	 * @param courseId Course ObjectId
	 * @param sectionIdsSorted Section ObjectIds array
	 */
	async sortSections(
		courseId: ObjectID,
		sectionIdsSorted: ObjectID[]
	): Promise<void> {
		const existingCourse = await this.findById(courseId);

		if (!existingCourse)
			throw new NotFoundException(CourseErrors.COURSE_NOT_FOUND);

		if (existingCourse.sections.length !== sectionIdsSorted.length)
			throw new BadRequestException(CourseErrors.SECTION_INVALID_REORDER);

		const newSections: ISectionDoc[] = [];

		for (const sectionId of sectionIdsSorted) {
			const section = (existingCourse.sections as ISectionDoc[]).find(
				i => i._id.toString() === sectionId
			);

			if (!section)
				throw new BadRequestException(CourseErrors.SECTION_INVALID_REORDER);

			newSections.push(section);
		}

		existingCourse.sections = newSections;
		await existingCourse.save();
	}

	/**
	 * Removes an existing course.
	 * - Cannot delete a course with sections.
	 * - Cannot delete a published course.
	 * @param courseId Course ObjectId
	 */
	async deleteCourse(courseId: ObjectID): Promise<void> {
		const existingCourse = await this.findById(courseId);

		if (!existingCourse)
			throw new NotFoundException(CourseErrors.COURSE_NOT_FOUND);

		if (existingCourse.visibility)
			throw new ConflictException(
				CourseErrors.COURSE_CANNOT_DELETE_WHEN_PUBLIC
			);

		if (existingCourse.sections.length > 0)
			throw new ConflictException(
				CourseErrors.COURSE_CANNOT_DELETE_WITH_SECTIONS
			);

		await existingCourse.remove().exec();
	}

	//#endregion

	//#region Course images

	/**
	 * Sets a new course main image.
	 * @param courseId Course Object Id
	 * @param filename Image path
	 * @returns New image url
	 */
	async setCourseImage(courseId: ObjectID, filename: string): Promise<string> {
		const course = await this.findById(courseId);
		if (!course) throw new NotFoundException(CourseErrors.COURSE_NOT_FOUND);

		if (course.image)
			await fspromises
				.unlink(
					resolve(
						FOLDER_UPLOADS,
						course.image.replace(
							this.configService.get(Env.SELF_DOMAIN) +
								this.configService.get(Env.UPLOADS_STATICS_PATH) +
								'/',
							''
						)
					)
				)
				.catch(error => Logger.error(error));
		course.image =
			this.configService.get(Env.SELF_DOMAIN) +
			this.configService.get(Env.UPLOADS_STATICS_PATH) +
			'/' +
			filename;
		await course.save();
		return course.image;
	}

	/**
	 * Sets a new course background image.
	 * @param courseId Course Object Id
	 * @param filename Image path
	 * @returns New image url
	 */
	async setCourseBgImage(
		courseId: ObjectID,
		filename: string
	): Promise<string> {
		const course = await this.findById(courseId);
		if (!course) throw new NotFoundException(CourseErrors.COURSE_NOT_FOUND);

		if (course.background)
			await fspromises
				.unlink(
					resolve(
						FOLDER_UPLOADS,
						course.background.replace(
							this.configService.get(Env.SELF_DOMAIN) +
								this.configService.get(Env.UPLOADS_STATICS_PATH) +
								'/',
							''
						)
					)
				)
				.catch(error => Logger.error(error));
		course.background =
			this.configService.get(Env.SELF_DOMAIN) +
			this.configService.get(Env.UPLOADS_STATICS_PATH) +
			'/' +
			filename;
		await course.save();
		return course.background;
	}

	//#endregion

	//#region Sections

	/**
	 * Create a new section within a course.
	 * @param courseId Course ObjectId
	 * @param input.title Section title
	 * @param input.description Section description
	 */
	async createSection(
		courseId: ObjectID,
		{ title, description }: SectionCreateDto
	): Promise<void> {
		const existingCourse = await this.findById(courseId);

		if (!existingCourse)
			throw new NotFoundException(CourseErrors.COURSE_NOT_FOUND);

		const newSection = { title, description, lessons: [], visibility: false };

		if (!existingCourse.sections) existingCourse.sections = [];

		existingCourse.sections.push(newSection);

		await existingCourse.save();
	}

	/**
	 * Modifies section data.
	 * @param courseId Course ObjectId
	 * @param sectionId Section ObjectId
	 * @param input.title Section new title
	 * @param input.description Section new description
	 */
	async modifySection(
		courseId: ObjectID,
		sectionId: ObjectID,
		{ title, description }: SectionModifyDto
	): Promise<void> {
		const existingCourse = await this.findById(courseId);

		if (!existingCourse)
			throw new NotFoundException(CourseErrors.COURSE_NOT_FOUND);

		if (!existingCourse.sections || !existingCourse.sections.length)
			throw new NotFoundException(CourseErrors.COURSE_DONT_HAVE_SECTIONS);

		const section = existingCourse.sections.find(
			i => i._id.toString() === sectionId
		);

		if (!section) throw new NotFoundException(CourseErrors.SECTION_NOT_FOUND);

		if (title) section.title = title;
		if (description) section.description = description;

		await existingCourse.save();
	}

	/**
	 * Sorts lessons in a course.
	 * @param courseId Course ObjectId
	 * @param sectionId Section ObjectId
	 * @param lessonIdsSorted Lesson ObjectIds array
	 */
	async sortLessons(
		courseId: ObjectID,
		sectionId: ObjectID,
		lessonIdsSorted: ObjectID[]
	): Promise<void> {
		const existingCourse = await this.findById(courseId);

		if (!existingCourse)
			throw new NotFoundException(CourseErrors.COURSE_NOT_FOUND);

		const section = existingCourse.sections.find(
			i => i._id.toString() === sectionId
		);

		if (!section) throw new NotFoundException(CourseErrors.SECTION_NOT_FOUND);

		if (section.lessons.length !== lessonIdsSorted.length)
			throw new BadRequestException(CourseErrors.LESSON_INVALID_REORDER);

		for (const lesson of section.lessons) {
			if (!lessonIdsSorted.includes(lesson.toString()))
				throw new BadRequestException(CourseErrors.SECTION_INVALID_REORDER);
		}

		section.lessons = lessonIdsSorted;
		await existingCourse.save();
	}

	/**
	 * Deletes a section from a course.
	 * - Cannot delete a public section.
	 * - Cannot delete a section with lessons.
	 * @param courseId Course ObjectId
	 * @param sectionId Section ObjectId
	 */
	async deleteSection(courseId: ObjectID, sectionId: ObjectID): Promise<void> {
		const existingCourse = await this.findById(courseId);

		if (!existingCourse)
			throw new NotFoundException(CourseErrors.COURSE_NOT_FOUND);

		if (!existingCourse.sections || !existingCourse.sections.length)
			throw new NotFoundException(CourseErrors.COURSE_DONT_HAVE_SECTIONS);

		const sectionIndex = existingCourse.sections.findIndex(
			i => i._id.toString() === sectionId
		);

		const section = existingCourse.sections[sectionIndex];

		if (!section) throw new NotFoundException(CourseErrors.SECTION_NOT_FOUND);

		if (section.visibility)
			throw new ConflictException(CourseErrors.SECTION_CANNOT_DELETE_PUBLIC);

		if (section.lessons.length > 0)
			throw new ConflictException(
				CourseErrors.SECTION_CANNOT_DELETE_WITH_LESSONS
			);

		existingCourse.sections.splice(sectionIndex, 1);

		await existingCourse.save();
	}

	//#endregion

	//#region Publish

	/**
	 * Publishes a course.
	 * - A published course must contain at least one published section.
	 * - All published sections of a course must contain at least one published lesson.
	 * @param courseId Course ObjectId
	 */
	async publish(courseId: ObjectID): Promise<void> {
		const existingCourse = await this.findByIdAndPopulate(courseId);

		if (!existingCourse)
			throw new NotFoundException(CourseErrors.COURSE_NOT_FOUND);

		if (existingCourse.visibility === true)
			throw new ConflictException(CourseErrors.COURSE_ALREADY_PUBLISHED);

		let publishedSections = 0;

		for (const section of existingCourse.sections) {
			if (section.visibility) {
				publishedSections++;

				let publishedLessons = 0;

				for (const lesson of section.lessons) {
					if (lesson.visibility) publishedLessons++;
				}

				if (publishedLessons === 0)
					throw new ConflictException(
						CourseErrors.COURSE_CANNOT_PUBLISH_WITH_PUBLIC_SECTION_WITHOUT_LESSONS
					);
			}
		}

		if (publishedSections === 0)
			throw new ConflictException(
				CourseErrors.COURSE_CANNOT_PUBLISH_WITHOUT_PUBLIC_SECTIONS
			);

		existingCourse.visibility = true;

		await existingCourse.save();
	}

	/**
	 * Unpublishes a course.
	 * - An unpublished course must not contain any student enrolled.
	 * @param courseId Course ObjectId.
	 */
	async unpublish(courseId: ObjectID): Promise<void> {
		const existingCourse = await this.findById(courseId);

		if (!existingCourse)
			throw new NotFoundException(CourseErrors.COURSE_NOT_FOUND);

		if (existingCourse.visibility === false)
			throw new ConflictException(CourseErrors.COURSE_ALREADY_PRIVATE);

		if (existingCourse.studentsCount)
			throw new ConflictException(
				CourseErrors.COURSE_CANNOT_UNPUBLISH_WITH_STUDENTS
			);

		existingCourse.visibility = false;

		await existingCourse.save();
	}

	/**
	 * Publishes a section.
	 * - A published section must contain at least one published lesson.
	 * @param courseId Course ObjectId
	 * @param sectionId Section ObjectId
	 */
	async publishSection(courseId: ObjectID, sectionId: ObjectID): Promise<void> {
		const existingCourse = await this.findByIdAndPopulate(courseId);

		if (!existingCourse)
			throw new NotFoundException(CourseErrors.COURSE_NOT_FOUND);

		if (!existingCourse.sections || !existingCourse.sections.length)
			throw new NotFoundException(CourseErrors.COURSE_DONT_HAVE_SECTIONS);

		const section = existingCourse.sections.find(
			i => i._id.toString() === sectionId
		);

		if (!section) throw new NotFoundException(CourseErrors.SECTION_NOT_FOUND);

		if (section.visibility === true)
			throw new ConflictException(CourseErrors.SECTION_ALREADY_PUBLISHED);

		let publishedLessons = 0;

		for (const lesson of section.lessons) {
			if (lesson.visibility) publishedLessons++;
		}

		if (publishedLessons === 0)
			throw new ConflictException(
				CourseErrors.SECTION_CANNOT_PUBLISH_WITHOUT_LESSONS
			);

		section.visibility = true;

		await existingCourse.save();
	}

	/**
	 * Unpublishes a section.
	 * - If a course is published, it must contain at least one published section.
	 * @param courseId Course ObjectId
	 * @param sectionId Section ObjectId
	 */
	async unpublishSection(
		courseId: ObjectID,
		sectionId: ObjectID
	): Promise<void> {
		const existingCourse = await this.findById(courseId);

		if (!existingCourse)
			throw new NotFoundException(CourseErrors.COURSE_NOT_FOUND);

		if (!existingCourse.sections || !existingCourse.sections.length)
			throw new NotFoundException(CourseErrors.COURSE_DONT_HAVE_SECTIONS);

		const section = existingCourse.sections.find(
			i => i._id.toString() === sectionId
		);

		if (!section) throw new NotFoundException(CourseErrors.SECTION_NOT_FOUND);

		if (section.visibility === false)
			throw new ConflictException(CourseErrors.SECTION_ALREADY_PRIVATE);

		if (existingCourse.visibility) {
			let publishedLessons = 0;

			for (const courseSection of existingCourse.sections) {
				if (courseSection.visibility) publishedLessons++;
			}

			if (publishedLessons === 0)
				throw new ConflictException(
					CourseErrors.SECTION_CANNOT_UNPUBLISH_ALL_WHEN_PUBLIC_COURSE
				);
		}

		section.visibility = false;

		await existingCourse.save();
	}

	//#endregion

	//#region Lessons

	/**
	 * Adds a new lesson to a section.
	 * @param course Course mongoose document
	 * @param sectionIndex Section ObjectId
	 * @param lessonId Lesson ObjectId
	 */
	async addLessonToSection(
		course: ICourseDoc,
		sectionIndex: number,
		lessonId: ObjectID
	): Promise<void> {
		course.sections[sectionIndex].lessons.push(lessonId);

		await course.save({ session: getCurrentSession() });
	}

	//#endregion

	//#region Students

	/**
	 * Enrolls a student in a course.
	 * @param course Course mongoose document
	 * @param studentId Student ObjectId
	 */
	async enrollStudentToCourse(
		course: ICourseDoc,
		studentId: ObjectID
	): Promise<void> {
		course.studentsEnrolled.push(studentId);

		await course.save();
	}

	/**
	 * Disenrolls a student in a course.
	 * @param course Course mongoose document
	 * @param studentId Student ObjectId
	 */
	async disenrollStudentFromCourse(
		course: ICourseDoc,
		studentId: ObjectID
	): Promise<void> {
		const index = course.studentsEnrolled.indexOf(studentId);

		course.studentsEnrolled.splice(index, 1);

		await course.save();
	}

	//#endregion

	//#region Workers

	/**
	 * Assing a worker to a course.
	 * @param courseId Course ObjectId
	 * @param workerId Student ObjectId
	 */
	@WithTransaction
	async asignWorkerToCourse(
		courseId: ObjectID,
		workerId: ObjectID
	): Promise<void> {
		const session = getCurrentSession();
		const course = (await this.courseModel
			.findById(courseId)
			.session(session)
			.exec()) as ICourseDoc;
		if (!course) throw new NotFoundException(CourseErrors.COURSE_NOT_FOUND);

		const worker = await this.workerService.findById(workerId);
		if (!worker) throw new NotFoundException(WorkerErrors.WORKER_NOT_FOUND);

		if (course.tutors.includes(workerId))
			throw new ConflictException(CourseErrors.WORKER_ALREADY_TUTOR);
		if (worker.teaches.includes(courseId))
			throw new ConflictException(WorkerErrors.WORKER_ALREADY_COURSE);

		course.tutors.push(workerId);

		await course.save({ session });
		await this.workerService.asignCourseToWorker(courseId, worker);
	}

	/**
	 * Unassing a worker from course.
	 * @param courseId Course ObjectId
	 * @param workerId Student ObjectId
	 */
	@WithTransaction
	async unassignWorkerFromCourse(
		courseId: ObjectID,
		workerId: ObjectID
	): Promise<void> {
		const session = getCurrentSession();
		const course = (await this.courseModel
			.findById(courseId)
			.session(session)
			.exec()) as ICourseDoc;
		if (!course) throw new NotFoundException(CourseErrors.COURSE_NOT_FOUND);

		const worker = await this.workerService.findById(workerId);
		if (!worker) throw new NotFoundException(WorkerErrors.WORKER_NOT_FOUND);

		if (!course.tutors.includes(workerId))
			throw new ConflictException(CourseErrors.WORKER_NOT_TUTOR);
		if (!worker.teaches.includes(courseId))
			throw new ConflictException(WorkerErrors.WORKER_NOT_COURSE);

		course.tutors = course.tutors.filter(
			tutor => tutor.toString() !== workerId
		);

		await course.save({ session });
		await this.workerService.unasignCourseToWorker(courseId, worker);
	}

	//#endregion
}
