import { AgendaModule } from '@Common/modules/agenda/agenda.module';
import { imageCourse, videoStorage } from '@Common/utils/file-upload';
import { forwardRef, Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { MulterModule } from '@nestjs/platform-express';
import { StudentsModule } from '@Students/students.module';
import { WorkersModule } from '@Workers/workers.module';
import { CourseController } from './controllers/course.controller';
import { CourseModels } from './enums/course-models.enum';
import { courseProviders } from './providers/course.providers';
import { lessonProviders } from './providers/lesson.providers';
import { CourseSchema } from './schemas/course.schema';
import {
	InfoLessonSchema,
	LessonSchema,
	VideoLessonSchema,
} from './schemas/lesson.schema';
import { CourseService } from './services/course.service';

@Module({
	imports: [
		MongooseModule.forFeature([
			{ name: CourseModels.COURSE, schema: CourseSchema },
			{
				name: CourseModels.LESSON,
				schema: LessonSchema,
				discriminators: [
					{
						name: CourseModels.LESSON_VIDEO,
						schema: VideoLessonSchema,
					},
					{
						name: CourseModels.LESSON_INFO,
						schema: InfoLessonSchema,
					},
				],
			},
		]),
		forwardRef(() => StudentsModule),
		forwardRef(() => WorkersModule),
		MulterModule.register({
			storage: videoStorage,
		}),
		MulterModule.register({
			storage: imageCourse,
		}),
		AgendaModule,
	],
	providers: [...courseProviders, ...lessonProviders],
	controllers: [CourseController],
	exports: [CourseService],
})
export class CoursesModule {}
