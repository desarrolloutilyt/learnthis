import { InputType, Field, Int } from '@nestjs/graphql';

@InputType()
export class SectionModifyDto {
	@Field({ nullable: true })
	title?: string;
	@Field({ nullable: true })
	description?: string;
}
