import { LessonTypes } from '@Courses/enums/lesson-types.enum';
import { Field, InputType } from '@nestjs/graphql';

@InputType()
export class LessonCreateDto {
	@Field()
	title: string;
	@Field()
	description: string;
	@Field()
	type: LessonTypes;
}
