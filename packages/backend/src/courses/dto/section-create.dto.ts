import { InputType, Field, Int } from '@nestjs/graphql';

@InputType()
export class SectionCreateDto {
	@Field()
	title: string;
	@Field()
	description: string;
}
