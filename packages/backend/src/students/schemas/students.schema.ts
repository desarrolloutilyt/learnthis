import { Gender } from '@Common/enums/gender.enum';
import { CourseModels } from '@Courses/enums/course-models.enum';
import { Course } from '@Courses/schemas/course.schema';
import { Lesson } from '@Courses/schemas/lesson.schema';
import { Prop, raw, Schema, SchemaFactory } from '@nestjs/mongoose';
import { OrderModels } from '@Orders/enums/order-models.enum';
import { Order } from '@Orders/schemas/order.schema';
import { SocialType } from '@Students/enums/social-type.enum';
import { Schema as MongooseSchema } from 'mongoose';

/** Social account possible properties */
type SocialAccountsProps = 'id' | 'type';

/** Social account interface */
interface ISocialAccountsProps {
	/** Social ID */
	id: string[];
	/** Social provider */
	type: string[];
}

/** Course enrolled possible properties */
type CourseEnrolledProps = 'course' | 'progress';

/** Course enrolled interface */
interface ICourseEnrolledProps {
	/** Course */
	course: Course;
	/** Progress in course */
	progress: {
		/** Current lesson */
		currentLesson: Lesson;
		/** List of course's lesson progress */
		lessons: {
			/** Lesson */
			lesson: Lesson;
			/** True if lesson completed */
			completed: boolean;
		}[];
	};
}

/**
 * Mongoose student's schema definition
 */
@Schema()
export class Student {
	/** Student's email */
	@Prop({ type: String, required: true, unique: true })
	email: string;

	/** Student's name */
	@Prop({ type: String, required: true })
	name: string;

	/** Student's surname */
	@Prop({ type: String, required: true })
	surname: string;

	/** Student's password */
	@Prop({ type: String })
	password: string;

	/** True if account active */
	@Prop({ type: Boolean, required: true, default: false })
	active: boolean;

	/** Student's username */
	@Prop({ type: String })
	username: string;

	/** Student's profile picture URL */
	@Prop({ type: String })
	photo: string;

	/** Student's biography */
	@Prop({ type: String })
	bio: string;

	/** Student's gender */
	@Prop({ type: String, enum: Object.values(Gender) })
	gender: Gender;

	/** Student's birth date */
	@Prop({ type: Date })
	birthDate: Date;

	/** Array of student's social accounts linked to account */
	@Prop(
		raw([
			{
				id: { type: String, required: true },
				type: { type: String, enum: Object.values(SocialType) },
			},
		])
	)
	socialAccounts: Record<SocialAccountsProps, ISocialAccountsProps>;

	/** Array of courses enrolled by student */
	@Prop(
		raw([
			{
				course: {
					type: MongooseSchema.Types.ObjectId,
					ref: CourseModels.COURSE,
					required: true,
				},
				progress: {
					currentLesson: {
						type: MongooseSchema.Types.ObjectId,
						ref: CourseModels.LESSON,
					},
					lessons: [
						{
							lesson: {
								type: MongooseSchema.Types.ObjectId,
								ref: CourseModels.LESSON,
								required: true,
							},
							completed: { type: Boolean, required: true, default: false },
						},
					],
				},
			},
		])
	)
	coursesEnrolled: Record<CourseEnrolledProps, ICourseEnrolledProps>;

	/** Array of orders purchased by student */
	@Prop({
		type: [{ type: MongooseSchema.Types.ObjectId, ref: OrderModels.ORDER }],
	})
	orders: Order[];

	/** Array of courses added to cart by student */
	@Prop({
		type: [{ type: MongooseSchema.Types.ObjectId, ref: CourseModels.COURSE }],
	})
	cart: Course[];
}

/**
 * Mongoose student's schema object
 */
export const StudentSchema = SchemaFactory.createForClass(Student);

StudentSchema.index({ email: 1 });
StudentSchema.index({ 'socialAccounts.id': 1, 'socialAccounts.type': 1 });
