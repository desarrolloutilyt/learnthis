import {
	fileFilter,
	FOLDER_UPLOADS,
	imageStorage,
} from '@Common/utils/file-upload';
import {
	Controller,
	Post,
	UploadedFile,
	UseGuards,
	UseInterceptors,
	BadRequestException,
} from '@nestjs/common';
import { FileInterceptor } from '@nestjs/platform-express';
import { GetRestAuthUser } from '@Common/auth/get-user.decorator';
import { StudentRestAuthGuard } from '@Students/auth/student-rest-auth.guard';
import { IStudentDoc } from '@Students/interfaces/student-document.interface';
import { StudentsService } from '@Students/services/students.service';
import { join } from 'path';
import { AgendaService } from '@Common/modules/agenda/services/agenda.service';

/**
 * Student upload photo controller
 */
@Controller('student')
export class StudentController {
	constructor(
		private studentService: StudentsService,
		private agendaService: AgendaService
	) {}

	/**
	 * Endpoint upload student photo
	 *
	 * @param  {} file
	 * @param  {IStudentDoc} student
	 * @returns Object
	 */
	@Post('upload')
	@UseGuards(StudentRestAuthGuard)
	@UseInterceptors(
		FileInterceptor('photo', {
			fileFilter,
			storage: imageStorage,
			limits: { fileSize: 4 * 1024 * 1024 },
		})
	)
	async upload(
		@UploadedFile() file,
		@GetRestAuthUser() student: IStudentDoc
	): Promise<{ url: string }> {
		if (!file) throw new BadRequestException();
		this.agendaService.enQueueUploadImageProcess(
			join(FOLDER_UPLOADS, file.filename)
		);
		return {
			url: await this.studentService.setPhoto(student, file.filename),
		};
	}
}
