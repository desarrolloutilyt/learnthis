import { CommonErrors } from '@Common/enums/common-errors.enum';
import { GenderValues } from '@Common/enums/gender.enum';
import { cleanObject, trimAllStrings } from '@Common/utils/clean-object';
import { BadRequestException, Injectable, PipeTransform } from '@nestjs/common';
import { StudentErrors } from '@Students/enums/student-errors.enum';
import { FormValidation } from 'learnthis-utils';
import { StudentModifyProfileDto } from '../dto/student-modify-profile.dto';

/**
 * StudentModifyProfileDto format validation and transform.
 */
@Injectable()
export class StudentModifyProfilePipe implements PipeTransform {
	/**
	 * Transform pipe.
	 * - Trim all strings.
	 * - Delete null or undefined properties.
	 * @param value
	 */
	transform(value: StudentModifyProfileDto): StudentModifyProfileDto {
		if (!Object.keys(value).length)
			throw new BadRequestException(CommonErrors.NOTHING_TO_MODIFY);

		const { name, surname, bio, birthDate, gender } = value;

		const errors = [];

		if (name && !FormValidation.nameValidation(name))
			errors.push(StudentErrors.FORMAT_NAME);

		if (surname && !FormValidation.nameValidation(surname))
			errors.push(StudentErrors.FORMAT_SURNAME);

		if (
			typeof bio === 'string' &&
			bio !== '' &&
			!FormValidation.bioValidation(bio)
		)
			errors.push(StudentErrors.FORMAT_BIO);

		if (birthDate && !FormValidation.birthDateValidation(birthDate))
			errors.push(StudentErrors.FORMAT_BIRTHDATE);

		if (gender && !GenderValues.includes(gender))
			errors.push(StudentErrors.FORMAT_GENDER);

		if (errors.length > 0) {
			throw new BadRequestException(errors.join('. '));
		}

		cleanObject(value);
		trimAllStrings(value);

		return value;
	}
}
