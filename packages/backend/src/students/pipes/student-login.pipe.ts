import { StudentErrors } from '@Students/enums/student-errors.enum';
import { BadRequestException, Injectable, PipeTransform } from '@nestjs/common';
import { FormValidation } from 'learnthis-utils';
import { StudentLoginDto } from '../dto/student-login.dto';

/**
 * StudentLoginDto format validation.
 */
@Injectable()
export class StudentLoginPipe implements PipeTransform {
	/**
	 * Validation pipe
	 * @param value Dto
	 * @returns Same dto
	 */
	transform(value: StudentLoginDto): StudentLoginDto {
		if (
			!FormValidation.emailValidation(value.email) ||
			!FormValidation.passwordValidation(value.password)
		)
			throw new BadRequestException(StudentErrors.INVALID_LOGIN);
		return value;
	}
}
