import { BadRequestException } from '@nestjs/common';
import { StudentChangePasswordDto } from '@Students/dto/student-change-password.dto';
import { StudentChangePasswordPipe } from './student-change-password.pipe';

describe('StudentChangePasswordPipe', () => {
	let changePasswordPipe: StudentChangePasswordPipe;

	beforeAll(() => (changePasswordPipe = new StudentChangePasswordPipe()));

	it('Contraseña correcta', () => {
		const passwords: StudentChangePasswordDto = {
			oldPassword: 'asd123ASD.',
			newPassword: 'ASDasd123.',
		};
		expect(changePasswordPipe.transform(passwords)).toBe(passwords);
	});

	it('Contraseña correcta sin anterior', () => {
		const passwords: StudentChangePasswordDto = {
			oldPassword: '',
			newPassword: 'ASDasd123.',
		};
		expect(changePasswordPipe.transform(passwords)).toBe(passwords);
	});

	it('Contraseña incorrecto - Nueva igual a anterior', () => {
		const passwords: StudentChangePasswordDto = {
			oldPassword: 'asd123ASD.',
			newPassword: 'asd123ASD.',
		};
		expect(() => changePasswordPipe.transform(passwords)).toThrow(
			BadRequestException
		);
	});

	it('Contraseña incorrecto - Anterior incorrecta', () => {
		const passwords: StudentChangePasswordDto = {
			oldPassword: 'asd12',
			newPassword: 'asd123ASD.',
		};
		expect(() => changePasswordPipe.transform(passwords)).toThrow(
			BadRequestException
		);
	});

	it('Contraseña incorrecto - Nueva incorrecta', () => {
		const passwords: StudentChangePasswordDto = {
			oldPassword: 'asd123ASD.',
			newPassword: 'asd12',
		};
		expect(() => changePasswordPipe.transform(passwords)).toThrow(
			BadRequestException
		);
	});
});
