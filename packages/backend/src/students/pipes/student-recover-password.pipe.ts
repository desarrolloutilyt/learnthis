import { StudentErrors } from '@Students/enums/student-errors.enum';
import { BadRequestException, Injectable, PipeTransform } from '@nestjs/common';
import { FormValidation, TokenValidation } from 'learnthis-utils';
import { StudentRecoverPasswordDto } from '../dto/student-recover-password.dto';

/**
 * StudentRecoverPasswordDto format validation.
 */
@Injectable()
export class StudentRecoverPasswordPipe implements PipeTransform {
	/**
	 * Validation pipe.
	 * @param value Dto
	 * @returns Same dto
	 */
	transform(value: StudentRecoverPasswordDto): StudentRecoverPasswordDto {
		const { newPassword, token } = value;

		if (!TokenValidation.validateJwt(token))
			throw new BadRequestException(StudentErrors.FORMAT_TOKEN);

		if (!FormValidation.passwordValidation(newPassword))
			throw new BadRequestException(StudentErrors.FORMAT_PASSWORD);

		return value;
	}
}
