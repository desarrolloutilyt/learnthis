import { InputType, Field } from '@nestjs/graphql';
import { SocialType } from '@Students/enums/social-type.enum';

/**
 * Student unlink soccial account.
 */
@InputType()
export class StudentSocialUnlinkDto {
	/** Social ID */
	@Field()
	id: string;
	/** Social provider */
	@Field()
	type: SocialType;
}
