import { InputType, Field } from '@nestjs/graphql';

/**
 * Student's change password
 */
@InputType()
export class StudentChangePasswordDto {
	/** Current password */
	@Field({ nullable: true })
	oldPassword: string;
	/** New password */
	@Field()
	newPassword: string;
}
