import { InputType, Field } from '@nestjs/graphql';

/**
 * Student standard email-password login.
 */
@InputType()
export class StudentLoginDto {
	/** Student's email */
	@Field()
	email: string;
	/** Student's password */
	@Field()
	password: string;
}
