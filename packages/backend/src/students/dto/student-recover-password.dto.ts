import { JwtToken } from '@Common/types/jwt-token.type';
import { InputType, Field, ID } from '@nestjs/graphql';

/**
 * Graphql input type: Student recover password.
 */
@InputType()
export class StudentRecoverPasswordDto {
	/** Recover password JWT token */
	@Field(() => ID)
	token: JwtToken;
	/** New student's password */
	@Field()
	newPassword: string;
}
