import { Gender } from '@Common/enums/gender.enum';
import { InputType, Field } from '@nestjs/graphql';

/**
 * Student's profile modification.
 *
 * Only must receive data on fields which want to modify.
 */
@InputType()
export class StudentModifyProfileDto {
	/** Student's name */
	@Field({ nullable: true })
	name?: string;

	/** Student's surname */
	@Field({ nullable: true })
	surname?: string;

	/** Student's biography */
	@Field({ nullable: true })
	bio?: string;

	/** Student's gender */
	@Field({ nullable: true })
	gender?: Gender;

	/** Student's birthdate */
	@Field({ nullable: true })
	birthDate?: Date;
}
