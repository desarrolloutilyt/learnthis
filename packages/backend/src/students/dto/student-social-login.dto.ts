import { InputType, Field } from '@nestjs/graphql';
import { SocialType } from '@Students/enums/social-type.enum';

/**
 * Student soccial login.
 */
@InputType()
export class StudentSocialLoginDto {
	/** Oauth token obtained from identity provider */
	@Field()
	token: string;
	/** Social provider */
	@Field()
	type: SocialType;
}
