import { InputType, Field } from '@nestjs/graphql';

/**
 * Student standard register.
 */
@InputType()
export class StudentRegisterDto {
	/** Student's email */
	@Field()
	email: string;
	/** Student's surname */
	@Field()
	name: string;
	/** Student's profile picture URL */
	@Field()
	surname: string;
	/** Student's password */
	@Field()
	password: string;
}
