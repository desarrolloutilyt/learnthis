import { Provider } from '@nestjs/common';
import { TokenService } from '@Students/services/student-token.service';

/**
 * Student token providers
 */
export const studentTokenProviders: Provider[] = [TokenService];
