import { Gender } from '@Common/enums/gender.enum';
import { ObjectID } from '@Common/types/object-id.type';
import { Field, ID, ObjectType } from '@nestjs/graphql';
import { SocialType } from '@Students/enums/social-type.enum';
import { ISocialAccount } from '@Students/interfaces/student-document.interface';

/**
 * Graphql type: Student
 */
@ObjectType()
export class Student {
	/** Mongo ObjectId */
	@Field(() => ID)
	_id: ObjectID;
	/** Student's email */
	email: string;
	/** True if account active */
	active?: boolean;
	/** Student's name */
	name: string;
	/** Student's surname */
	surname: string;
	/** Student's profile picture URL */
	photo?: string;
	/** Student's biography */
	bio?: string;
	/** Student's username */
	username?: string;
	/** Student's gender */
	gender?: Gender;
	/** Student's birth date */
	birthDate?: Date;
	/** Array of student's social accounts linked to account */
	socialAccounts?: SocialAccount[];
	/** Array of courses added to cart by student */
	cart?: ObjectID[];
	/** Array of courses enrolled by student */
	coursesEnrolled?: CourseEnroll[];
	/** Array of orders purchased by student */
	orders?: ObjectID[];
}

/**
 * Graphql type: Social account
 */
@ObjectType()
class SocialAccount implements ISocialAccount {
	/** Social ID */
	@Field(() => ID)
	id: ObjectID;
	/** Social account provider */
	type: SocialType;
}

// TODO: Este tipo creo que no tiene sentido
@ObjectType()
class CourseEnroll {
	course: ObjectID;
}
