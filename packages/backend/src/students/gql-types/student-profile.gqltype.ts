import { ObjectType } from '@nestjs/graphql';
import { Student } from './student.gqltype';

/**
 * Graphql type: Student's profile
 */
@ObjectType()
export class StudentProfile extends Student {
	isSocialLogin: boolean;
}
