import { getCurrentSession } from '@Common/decorators/transactional';
import { CommonErrors } from '@Common/enums/common-errors.enum';
import { Env } from '@Common/enums/env.enum';
import { ObjectIdTokenPayload } from '@Common/types/objectid-token-payload.type';
import { BadRequestException, HttpService, Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { JwtService } from '@nestjs/jwt';
import { InjectModel } from '@nestjs/mongoose';
import { SocialType } from '@Students/enums/social-type.enum';
import { StudentModels } from '@Students/enums/student-models.enum';
import { StudentTokenTypes } from '@Students/enums/student-token-types.enum';
import {
	ISocialAccount,
	IStudent,
} from '@Students/interfaces/student-document.interface';
import { TokenDocument } from '@Students/interfaces/token-document.interface';
import { GithubProfileInfo } from '@Students/types/github-profile-info.type';
import { GitlabProfileInfo } from '@Students/types/gitlab-profile-info.type';
import { GoogleProfileInfo } from '@Students/types/google-profile-info.type';
import { SocialAccountInfo } from '@Students/types/social-account-info.type';
import { Model } from 'mongoose';
import { forkJoin } from 'rxjs';
import { map } from 'rxjs/operators';

/**
 * Service to manage database operations on the students token table.
 */
@Injectable()
export class TokenService {
	/**
	 * Dependency injection.
	 * @param tokenModel Student tokens mongoose entity
	 * @param configService Config service
	 * @param jwtService JWT service
	 * @param httpService HTTP service
	 */
	constructor(
		@InjectModel(StudentModels.TOKEN)
		private readonly tokenModel: Model<TokenDocument>,
		private readonly configService: ConfigService,
		private readonly jwtService: JwtService,
		private readonly httpService: HttpService
	) {}

	/**
	 * Tokens expiration definition by type
	 */
	private readonly TOKEN_EXPIRATION = {
		[StudentTokenTypes.ACTIVATE_TOKEN]:
			1000 *
			60 *
			60 *
			24 *
			Number(this.configService.get(Env.STUDENT_TOKEN_ACTIVATE_LIFE)),
		[StudentTokenTypes.RECOVER_TOKEN]:
			1000 *
			60 *
			60 *
			24 *
			Number(this.configService.get(Env.STUDENT_TOKEN_RECOVER_LIFE)),
	};

	/**
	 * JWT token sign generator and save to db
	 *
	 * @param  {string} studentId
	 * @param  {StudentTokenTypes} type
	 * @returns Promise
	 */
	async generateToken(
		studentId: string,
		type: StudentTokenTypes
	): Promise<string> {
		const expiration = this.TOKEN_EXPIRATION[type];

		const payload: ObjectIdTokenPayload = {
			id: studentId,
		};

		const token = await this.jwtService.signAsync(payload, {
			expiresIn: expiration,
			secret: this.configService.get(Env.STUDENT_TOKEN_KEY),
		});

		await this.tokenModel.create(
			[
				{
					token,
					type,
					student: studentId,
					expiresAt: new Date(Date.now() + expiration),
				},
			],
			{ session: getCurrentSession() }
		);

		return token;
	}

	/**
	 * JWT token validation and check with db
	 *
	 * @param  {string} token
	 * @returns Promise
	 */
	async checkToken(token: string): Promise<ObjectIdTokenPayload> {
		const payload: ObjectIdTokenPayload | null = await this.jwtService
			.verifyAsync(token, {
				secret: this.configService.get(Env.STUDENT_TOKEN_KEY),
			})
			.catch(e => null);
		const tokenFound = await this.tokenModel
			.findOne({ token })
			.session(getCurrentSession())
			.exec();
		if (tokenFound && payload) return payload;

		throw new BadRequestException(CommonErrors.TOKEN_NOT_FOUND);
	}

	/**
	 * Token db remove method
	 *
	 * @param  {string} token
	 * @returns Promise
	 */
	async removeToken(token: string): Promise<boolean> {
		const result = await this.tokenModel
			.deleteOne({ token }, { session: getCurrentSession() })
			.exec();
		if (result.n !== 1 || result.ok !== 1)
			throw new BadRequestException(CommonErrors.TOKEN_NOT_FOUND);
		return true;
	}

	/**
	 * Get Google Profile info from acces token
	 *
	 * @param  {string} accessToken
	 * @returns Promise
	 */
	getGoogleAccessTokenInfo(accessToken: string): Promise<SocialAccountInfo> {
		return this.httpService
			.get<GoogleProfileInfo>('https://www.googleapis.com/oauth2/v2/userinfo', {
				headers: {
					Authorization: `Bearer ${accessToken}`,
				},
			})
			.pipe(
				map(data => ({
					socialAccount: {
						id: data.data.id,
						type: SocialType.GOOGLE,
					} as ISocialAccount,
					studentData: {
						email: data.data.email,
						name: data.data.given_name,
						surname: data.data.family_name,
						active: true,
						photo: data.data.picture,
						socialAccounts: [],
						cart: [],
						coursesEnrolled: [],
						orders: [],
					} as IStudent,
				}))
			)
			.toPromise();
	}

	getGitlabAccessTokenInfo(accessToken: string): Promise<SocialAccountInfo> {
		return this.httpService
			.get<GitlabProfileInfo>(`https://gitlab.com/api/v4/user`, {
				headers: {
					Authorization: `Bearer ${accessToken}`,
				},
			})
			.pipe(
				map(data => ({
					socialAccount: {
						id: data.data.id.toString(),
						type: SocialType.GITLAB,
					} as ISocialAccount,
					studentData: {
						email: data.data.email,
						name: data.data.name,
						surname: data.data.name,
						username: data.data.username,
						active: true,
						photo: data.data.avatar_url,
						socialAccounts: [],
						cart: [],
						coursesEnrolled: [],
						orders: [],
					} as IStudent,
				}))
			)
			.toPromise();
	}

	getGithubAccessTokenInfo(accessToken: string): Promise<SocialAccountInfo> {
		return forkJoin([
			this.httpService
				.get<GithubProfileInfo>('https://api.github.com/user', {
					headers: {
						Authorization: `token ${accessToken}`,
					},
				})
				.pipe(map(data => data.data)),
			this.httpService
				.get('https://api.github.com/user/emails', {
					headers: {
						Authorization: `token ${accessToken}`,
					},
				})
				.pipe(map(data => data.data[0].email)),
		])
			.pipe(
				map<[GithubProfileInfo, string], SocialAccountInfo>(arr => ({
					socialAccount: {
						id: arr[0].id.toString(),
						type: SocialType.GITHUB,
					} as ISocialAccount,
					studentData: {
						email: arr[1],
						name: arr[0].name || arr[0].login,
						surname: arr[0].name || arr[0].login,
						active: true,
						photo: arr[0].avatar_url,
						socialAccounts: [],
						cart: [],
						coursesEnrolled: [],
						orders: [],
					} as IStudent,
				}))
			)
			.toPromise();
	}
}
