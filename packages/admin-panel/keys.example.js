export const BACK_URL = 'http://localhost:3000';
export const GQL_ENDPOINT = BACK_URL + '/graphql';
export const WORKERS_UPLOAD = BACK_URL + '/worker';
export const COURSE_UPLOAD = BACK_URL + '/course';
