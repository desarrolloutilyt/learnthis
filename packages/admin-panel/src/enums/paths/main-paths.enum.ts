export enum MainParams {
	WORKER_ID = ':workerId',
	COURSE_ID = ':courseId',
	SECTION_ID = ':sectionId',
	LESSON_ID = ':lessonId',
}

type UrlParamValues = 'workerId' | 'courseId' | 'sectionId' | 'lessonId';

export type UrlParams = {
	[key in UrlParamValues]: string;
};

export enum MainPaths {
	DASHBOARD = '/',
	//#region USERS
	WORKERS_LIST = '/workers',
	WORKERS = '/workers/:workerId',
	WORKER_CREATE = '/workers/create',
	WORKER_DATA = '/workers/:workerId/data',
	WORKER_ADVANCED = '/workers/:workerId/advanced',
	WORKER_COURSES = '/workers/:workerId/courses',
	//#endregion
	//#region COURSES
	COURSES_LIST = '/courses',
	COURSE = '/courses/:courseId',
	COURSE_CREATE = '/courses/create',
	COURSE_CREATE_SECTION = '/courses/create-section',
	COURSE_DATA = '/courses/:courseId/data',
	COURSE_SECTIONS = '/courses/:courseId/sections',
	COURSE_TUTORS = '/courses/:courseId/tutors',
	COURSE_ADVANCED = '/courses/:courseId/advanced',

	SECTION = '/courses/:courseId/:sectionId',
	SECTION_DATA = '/courses/:courseId/:sectionId/data',
	SECTION_LESSONS = '/courses/:courseId/:sectionId/lessons',
	SECTION_ADVANCED = '/courses/:courseId/:sectionId/advanced',
	SECTION_CREATE_LESSON = '/courses/:courseId/:sectionId/register',

	LESSON = '/courses/lesson/:lessonId',
	LESSON_DATA = '/courses/lesson/:lessonId/data',
	LESSON_ADVANCED = '/courses/lesson/:lessonId/advanced',

	//#endregion
	CLIENTS_LIST = '/clients',

	ORDERS = '/orders',
	STATS = '/stats',
	LOGIN = '/login',
	NOT_FOUND = '/',
}
