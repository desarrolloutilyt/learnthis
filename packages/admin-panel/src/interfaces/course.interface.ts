import { ISection } from './section.interface';

export interface ICourse {
	_id: string;
	url: string;
	title: string;
	description: string;
	price: number;
	compareAtPrice?: number;
	image: string;
	background?: string;
	studentsCount: number;
	visibility: boolean;
	sections: ISection[];
}
