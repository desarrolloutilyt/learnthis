import { WorkerRoles } from '@Enums/worker/worker-roles.enum';

export interface IWorker {
	_id: string;
	email: string;
	name: string;
	surname: string;
	displayName: string;
	active: boolean;
	username?: string;
	phone?: string;
	photo?: string;
	bio?: string;
	roles: WorkerRoles[];
	teaches: string[];
}
