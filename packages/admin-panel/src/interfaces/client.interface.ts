export interface IStudent {
	_id: string;
	active: boolean;
	coursesEnrolled: string[];
	orders: string[];
}
