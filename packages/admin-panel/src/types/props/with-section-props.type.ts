import { ApolloQueryRefetch } from '@Interfaces/apollo/apollo-refetch.types';
import { ICourse } from '@Interfaces/course.interface';
import { ISection } from '@Interfaces/section.interface';

export type SectionWrapperProps = {
	course: ICourse | null;
	sectionId: string;
	section: ISection | null;
	refetch: ApolloQueryRefetch;
};
