import { FC } from 'react';

type TitleCardProps = {
	title: string;
	description?: string;
	className: string;
};

const TitleCard: FC<TitleCardProps> = ({
	title,
	description,
	className = '',
	children,
}) => {
	return (
		<div className={`p-1 ${className}`}>
			<div
				className={`bg-white dark:bg-white-dark rounded-2xl p-1_5 shadow-md`}>
				<div className='px-1 pb-2'>
					<h2 className='text-lg font-semibold text-black dark:text-white'>
						{title}
					</h2>
					{description && <p className='text-gray'>{description}</p>}
				</div>
				{children}
			</div>
		</div>
	);
};

export default TitleCard;
