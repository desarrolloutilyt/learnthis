import { FC } from 'react';

type BlankCardProps = {
	className: string;
};

const BlankCard: FC<BlankCardProps> = ({ className = '', children }) => {
	return (
		<div className={`p-0_75 ${className}`}>
			<div
				className={`bg-white dark:bg-white-dark rounded-2xl p-1_5 shadow-md`}>
				{children}
			</div>
		</div>
	);
};

export default BlankCard;
