import BlankCard from '@Components/generic/cards/blank-card';
import PlugIcon from '@Components/icons/other/plug-icon';
import { FC } from 'react';

const Error404: FC = () => (
	<div className='w-full max-w-25 mx-auto py-2'>
		<BlankCard className='w-full'>
			<div className='flexcol-c-c text-center py-1_5'>
				<PlugIcon className='h-5 w-5 text-primary' />
				<h1 className='font-semibold text-xl mt-2 text-black dark:text-white'>
					Página no encontrada
				</h1>
				<p className='text-gray w-full max-w-15 mt-1'>
					No sabemos qué andabas buscando, pero no está aquí...
				</p>
			</div>
		</BlankCard>
	</div>
);

export default Error404;
