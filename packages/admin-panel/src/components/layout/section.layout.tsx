import { FC } from 'react';

export type SectionLayoutProps = {
	title: string;
};

const SectionLayout: FC<SectionLayoutProps> = ({ title, children }) => {
	return (
		<div className='container-lg py-2'>
			<h1 className='text-3xl px-1_5 pb-1_5 text-center font-bold text-black dark:text-white'>
				{title}
			</h1>
			<div className='flex flex-wrap'>{children}</div>
		</div>
	);
};

export default SectionLayout;
