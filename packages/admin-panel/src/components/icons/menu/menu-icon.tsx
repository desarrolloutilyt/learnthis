import { ComponentProps, FC } from 'react';

const MenuIcon: FC<ComponentProps<'svg'>> = props => (
	<svg {...props} fill='none' stroke='currentColor' viewBox='0 0 24 24'>
		<path
			strokeLinecap='round'
			strokeLinejoin='round'
			strokeWidth={2}
			d='M4 6h16M4 12h16M4 18h16'
		/>
	</svg>
);

export default MenuIcon;
