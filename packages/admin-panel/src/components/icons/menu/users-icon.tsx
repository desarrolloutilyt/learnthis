import { ComponentProps, FC } from 'react';

const UsersIcon: FC<ComponentProps<'svg'>> = props => (
	<svg
		{...props}
		width='24'
		height='24'
		viewBox='0 0 24 24'
		strokeWidth='2'
		stroke='currentColor'
		fill='none'
		strokeLinecap='round'
		strokeLinejoin='round'>
		{' '}
		<path stroke='none' d='M0 0h24v24H0z' /> <circle cx='12' cy='7' r='4' />{' '}
		<path d='M6 21v-2a4 4 0 0 1 4 -4h4a4 4 0 0 1 4 4v2' />
	</svg>
);

export default UsersIcon;
