import { InfoIcon } from '@Components/icons/lessons/info-icon';
import { VideoIcon } from '@Components/icons/lessons/video-icon';
import { LessonTypes } from '@Enums/course/lesson-types.enum';
import { MainPaths } from '@Enums/paths/main-paths.enum';
import { ILesson } from '@Interfaces/lesson.interface';
import { formatDuration } from '@Lib/utils/date.utils';
import { FC } from 'react';
import { Link } from 'react-router-dom';
import CoursePublishedSpan from '../course-published-span';
import SectionRow from './section-row';

type SectionTableProps = {
	lessons: ILesson[] | null;
	baseUrl: string;
};

const SectionTable: FC<SectionTableProps> = ({ lessons, baseUrl }) => {
	const rows = mapLessons(lessons, baseUrl);
	return (
		<div className='w-full max-w-30 mx-auto border border-gray-light dark:border-gray-dark rounded-xl py-0_25'>
			{rows}
		</div>
	);
};

const mapLessons = (lessons: ILesson[] | null, baseUrl: string) => {
	if (!lessons) return null;

	return lessons.map((i, index) => (
		<Link
			to={baseUrl.replace(':lessonId', i._id)}
			key={index}
			className='flex px-1 hover:bg-gray-light dark:hover:bg-gray-dark'>
			<SectionRow lesson={i} index={index} length={lessons.length} />
		</Link>
	));
};

export default SectionTable;
