import {
	courseLinks,
	lessonLinks,
	sectionLinks,
} from '@Config/nav-links.settings';
import { NavTypes } from '@Enums/course/nav.types.enum';
import { MainParams } from '@Enums/paths/main-paths.enum';
import { FC } from 'react';
import { NavLink } from 'react-router-dom';
import { CustomNavLink } from 'src/types/icon-nav-link.type';

type CourseNavProps = {
	type: NavTypes;
	courseId: string;
	sectionId?: string;
	lessonId?: string;
};

const CoursesNav: FC<CourseNavProps> = ({
	type,
	courseId,
	sectionId,
	lessonId,
}) => {
	let links = getLinks(type, courseId, sectionId, lessonId);

	const linkItems = mapLinks(links);
	return (
		<div className='w-full px-0_5 xssm:order-1 mb-1'>
			<div className='flex-c-c'>{linkItems}</div>
		</div>
	);
};

const getLinks = (
	type: NavTypes,
	courseId: string,
	sectionNum?: string,
	lessonId?: string
): Array<CustomNavLink> => {
	let links: Array<CustomNavLink> = [];

	if (type === NavTypes.COURSE)
		links = courseLinks.map(({ ...link }) => {
			link.to = link.to.replace(MainParams.COURSE_ID, courseId);
			return link;
		});
	else if (type === NavTypes.SECTION)
		links = sectionLinks.map(({ ...link }) => {
			link.to = link.to
				.replace(MainParams.COURSE_ID, courseId)
				.replace(MainParams.SECTION_ID, sectionNum || '');
			return link;
		});
	else if (type === NavTypes.LESSON)
		links = lessonLinks.map(({ ...link }) => {
			link.to = link.to.replace(MainParams.LESSON_ID, lessonId || '');
			return link;
		});

	return links;
};

const mapLinks = (links: CustomNavLink[]) =>
	links.map(({ to, exact, text }, index) => (
		<NavLink
			key={index}
			to={to}
			exact={exact}
			className='py-0_5 px-1_25 mx-0_5 text-gray font-semibold rounded-lg hover:text-white hover:bg-primary'
			activeClassName='bg-primary text-white'>
			<span>{text}</span>
		</NavLink>
	));

export default CoursesNav;
