import ImageDragDrop from '@Components/generic/image-drag-drop';
import { MainPaths } from '@Enums/paths/main-paths.enum';
import { COURSE_UPLOAD } from '@Keys';
import { CourseWrapperProps } from '@Types/props/with-course-props.type';
import { CSSProperties, FC } from 'react';
import { Route, Switch } from 'react-router-dom';

const CourseHeader: FC<CourseWrapperProps> = ({ course, refetch }) => {
	const bgImageStyles: CSSProperties | undefined = course?.background
		? {
				backgroundImage: `url('${course?.background}')`,
				backgroundSize: 'cover',
				backgroundPosition: 'center',
				backgroundRepeat: 'no-repeat',
		  }
		: undefined;

	const courseImageStyles: CSSProperties | undefined = course?.image
		? {
				backgroundImage: `url('${course?.image}')`,
				backgroundSize: 'cover',
				backgroundPosition: 'center',
				backgroundRepeat: 'no-repeat',
		  }
		: undefined;

	return (
		<div className='w-full mb-6 shadow-md rounded-2xl relative'>
			<div className='w-full h-12 rounded-2xl overflow-hidden relative'>
				<Switch>
					<Route
						path={MainPaths.COURSE_DATA}
						render={() => (
							<ImageDragDrop
								current={course?.background}
								name='bgImage'
								url={`${COURSE_UPLOAD}/${course?._id}/bgImage`}
								ratio={19 / 4}
								onChange={() => refetch()}
							/>
						)}
					/>
					<Route
						path={MainPaths.COURSE}
						render={() => (
							<div
								style={bgImageStyles}
								className='w-full h-full absolute top-0 pointer-events-none'
							/>
						)}
					/>
				</Switch>
			</div>
			<div className='w-10 h-10 overflow-hidden absolute top-8 left-2 rounded-full'>
				<Switch>
					<Route
						path={MainPaths.COURSE_DATA}
						render={() => (
							<ImageDragDrop
								current={course?.image}
								name='image'
								rounded
								url={`${COURSE_UPLOAD}/${course?._id}/image`}
								onChange={() => refetch()}
							/>
						)}
					/>
					<Route
						path={MainPaths.COURSE}
						render={() => (
							<div
								style={courseImageStyles}
								className='w-full h-full absolute top-0 pointer-events-none'
							/>
						)}
					/>
				</Switch>
			</div>
		</div>
	);
};

export default CourseHeader;
