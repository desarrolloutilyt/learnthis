import { ApolloError, useMutation } from '@apollo/client';
import Button from '@Components/generic/form/button/button';
import Input from '@Components/generic/form/input';
import TextArea from '@Components/generic/form/textarea';
import SectionLayout from '@Components/layout/section.layout';
import { AlertMessages, FormMessages } from '@Enums/config/constants';
import { MainPaths } from '@Enums/paths/main-paths.enum';
import { WorkerRoles } from '@Enums/worker/worker-roles.enum';
import { ApolloMutation } from '@Interfaces/apollo/apollo-query.types';
import { withAuth } from '@Lib/hoc/withAuth';
import { Form, Formik, FormikConfig } from 'formik';
import { GraphqlCourse } from 'learnthis-utils';
import { Dispatch, FC, SetStateAction, useState } from 'react';
import { Prompt, useHistory } from 'react-router-dom';
import { toast } from 'react-toastify';
import {
	number as YupNumber,
	object as YupObject,
	string as YupString,
} from 'yup';

//#region TS

/**
 * Enum for form field names
 */
enum CourseCreateFields {
	URL = 'reg_url',
	TITLE = 'reg_title',
	DESCRIPTION = 'reg_description',
	PRICE = 'reg_price',
}

/**
 * Interface for form field values
 */
interface ICourseCreateInput {
	[CourseCreateFields.URL]: string;
	[CourseCreateFields.TITLE]: string;
	[CourseCreateFields.DESCRIPTION]: string;
	[CourseCreateFields.PRICE]: number;
}

//#endregion

/**
 * Page component to register a new course.
 *
 * Authenticated: YES
 * Allowed roles: Admin
 */
const PageCourseCreate: FC = () => {
	const [isDone, setIsDone] = useState<boolean>(false);

	const { registerCourseMutation, loading } = getRegisterCourseMutation(
		setIsDone
	);

	const formikConfig = getForm(registerCourseMutation);

	return (
		<SectionLayout title='Cursos'>
			<div className='container-sm m-1_5 bg-white dark:bg-white-dark p-1_5 rounded-2xl shadow-md'>
				<Formik {...formikConfig}>
					{({ dirty }) => (
						<Form className='w-full mx-auto flex-c-s flex-wrap'>
							<Prompt
								when={dirty && !isDone}
								message='¿Estás seguro de que deseas salir? Perderás todos los cambios'
							/>
							<h1 className='w-full p-1 font-semibold text-xl text-center textblack dark:text-white'>
								Registro de curso
							</h1>
							<Input
								name={CourseCreateFields.TITLE}
								className='w-full p-0_5'
								type='text'
								label='Título'
							/>
							<Input
								name={CourseCreateFields.URL}
								className='w-6/12 p-0_5'
								type='text'
								label='Url'
							/>
							<Input
								name={CourseCreateFields.PRICE}
								className='p-0_5 w-6/12'
								type='number'
								label='Precio'
								step='any'
								min={0}
							/>
							<TextArea
								name={CourseCreateFields.DESCRIPTION}
								className='p-0_5 w-full'
								label='Descripción'
								rows={5}
							/>

							<Button
								loading={loading}
								className='mt-2 p-0_75 w-10'
								type='submit'
								kind='primary'>
								Siguiente
							</Button>
						</Form>
					)}
				</Formik>
			</div>
		</SectionLayout>
	);
};

/**
 * Gets the graphql mutation to register a new course.
 */
const getRegisterCourseMutation = (
	setIsDone: Dispatch<SetStateAction<boolean>>
) => {
	const history = useHistory();

	const [registerCourseMutation, { loading }] = useMutation(
		GraphqlCourse.course_admin_create,
		{
			onCompleted: (data: any) => {
				toast.success(AlertMessages.REGISTER_COURSE_SUCCESS);
				setIsDone(true);
				history.push(MainPaths.COURSE.replace(':id', data.course_admin_create));
			},
			onError: (error: ApolloError) => {
				toast.error(error.message || AlertMessages.SERVER_ERROR);
			},
		}
	);

	return { registerCourseMutation, loading };
};

/**
 * Gets the formik data to build the form.
 * @param {ApolloMutation} registerMutation Graphql mutation
 */
const getForm = (
	registerCourseMutation: ApolloMutation
): FormikConfig<ICourseCreateInput> => {
	const initialValues = {
		[CourseCreateFields.URL]: '',
		[CourseCreateFields.TITLE]: '',
		[CourseCreateFields.DESCRIPTION]: '',
		[CourseCreateFields.PRICE]: 0,
	};

	const validationSchema = YupObject().shape({
		[CourseCreateFields.URL]: YupString().required(FormMessages.URL_REQUIRED),
		[CourseCreateFields.TITLE]: YupString().required(
			FormMessages.TITLE_REQUIRED
		),
		[CourseCreateFields.DESCRIPTION]: YupString().required(
			FormMessages.DESCRIPTION_REQUIRED
		),
		[CourseCreateFields.PRICE]: YupNumber()
			.positive(FormMessages.MIN_PRICE)
			.required(FormMessages.PASSWORD_REQUIRED),
	});

	const onSubmit = (values: ICourseCreateInput) => {
		registerCourseMutation({
			variables: {
				courseData: {
					url: values[CourseCreateFields.URL],
					title: values[CourseCreateFields.TITLE],
					description: values[CourseCreateFields.DESCRIPTION],
					price: values[CourseCreateFields.PRICE],
				},
			},
		});
	};

	return {
		initialValues,
		validationSchema,
		onSubmit,
		validateOnChange: false,
	};
};

export default withAuth(PageCourseCreate, [WorkerRoles.ADMIN]);
