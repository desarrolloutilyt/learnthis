import { WorkerRoles } from '@Enums/worker/worker-roles.enum';
import { withAuth } from '@Lib/hoc/withAuth';
import { FC } from 'react';

const PageSectionCreate: FC = () => {
	return <></>;
};

export default withAuth(PageSectionCreate, [WorkerRoles.ADMIN]);
