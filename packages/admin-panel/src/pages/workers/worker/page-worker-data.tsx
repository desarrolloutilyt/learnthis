import { ApolloError, useMutation } from '@apollo/client';
import Button from '@Components/generic/form/button/button';
import Input from '@Components/generic/form/input';
import TextArea from '@Components/generic/form/textarea';
import ImageDragDrop from '@Components/generic/image-drag-drop';
import { AlertMessages, FormMessages } from '@Enums/config/constants';
import { MainParams, MainPaths } from '@Enums/paths/main-paths.enum';
import { ApolloMutation } from '@Interfaces/apollo/apollo-query.types';
import { ApolloQueryRefetch } from '@Interfaces/apollo/apollo-refetch.types';
import { WorkerProfileProps } from '@Interfaces/props/worker-profile-props.interface';
import { IWorker } from '@Interfaces/worker.interface';
import { WORKERS_UPLOAD } from '@Keys';
import { AuthContext, IAuthState } from '@Lib/context/auth.context';
import { Form, Formik, FormikConfig } from 'formik';
import { FormValidation, GraphqlWorker } from 'learnthis-utils';
import { Dispatch, FC, SetStateAction, useContext, useState } from 'react';
import { Prompt, useHistory } from 'react-router-dom';
import { toast } from 'react-toastify';
import { object as YupObject, string as YupString } from 'yup';

//#region TS

/**
 * Enum for form field names
 */
enum WorkerDataFields {
	EMAIL = 'email',
	NAME = 'name',
	SURNAME = 'surname',
	DISPLAY_NAME = 'displayName',
	USERNAME = 'username',
	BIO = 'bio',
	PHONE = 'phone',
}

/**
 * Interface for form field values
 */
interface IWorkerDataInput {
	[WorkerDataFields.EMAIL]: string;
	[WorkerDataFields.NAME]: string;
	[WorkerDataFields.SURNAME]: string;
	[WorkerDataFields.DISPLAY_NAME]: string;
	[WorkerDataFields.USERNAME]: string;
	[WorkerDataFields.BIO]: string;
	[WorkerDataFields.PHONE]: string;
}

//#endregion

/**
 * Subpage component to edit worker data.
 *
 * Authenticated: YES
 * Allowed roles: Admin
 */
const PageWorkerData: FC<WorkerProfileProps> = ({ worker, refetch }) => {
	const [isDone, setIsDone] = useState<boolean>(false);
	const { authState, updateUser } = useContext(AuthContext);

	const { modifyUserMutation, loading } = getModifyUserMutation(
		worker._id,
		refetch,
		setIsDone,
		authState,
		updateUser
	);

	const url = `${WORKERS_UPLOAD}/${
		authState.worker?._id === worker._id ? 'image' : `${worker._id}/admin/image`
	}`;

	const formikConfig = getForm(modifyUserMutation, worker);

	return (
		<>
			<div className='flex-c-c'>
				<div className='w-8 h-8 overflow-hidden rounded-full'>
					<ImageDragDrop
						current={worker.photo}
						name='image'
						url={url}
						onChange={url => {
							if (authState.worker?._id === worker._id) {
								updateUser({
									photo: url,
								});
							}
							refetch();
						}}
						rounded={true}
					/>
				</div>
			</div>

			<Formik {...formikConfig}>
				{({ dirty }) => (
					<Form className='w-full max-w-24 mx-auto'>
						<Prompt
							when={dirty && !isDone}
							message='¿Estás seguro de que deseas salir? Perderás todos los cambios'
						/>
						<Input
							name={WorkerDataFields.EMAIL}
							className='mt-1'
							type='email'
							label='Email'
						/>
						<Input
							name={WorkerDataFields.DISPLAY_NAME}
							className='mt-1'
							type='text'
							label='Nombre para mostrar'
						/>
						<Input
							name={WorkerDataFields.NAME}
							className='mt-1'
							type='text'
							label='Nombre'
						/>
						<Input
							name={WorkerDataFields.SURNAME}
							className='mt-1'
							type='text'
							label='Apellidos'
						/>
						<Input
							name={WorkerDataFields.USERNAME}
							className='mt-1'
							type='text'
							label='Username'
						/>
						<Input
							name={WorkerDataFields.PHONE}
							className='mt-1'
							type='text'
							label='Teléfono'
						/>
						<TextArea
							name={WorkerDataFields.BIO}
							className='mt-1'
							label='Bio'
							rows={4}
						/>
						<div className='flex-c-c'>
							<Button
								loading={loading}
								className='mt-2 p-0_75 w-10'
								type='submit'
								kind='primary'>
								Actualizar
							</Button>
						</div>
					</Form>
				)}
			</Formik>
		</>
	);
};

/**
 * Gets the graphql mutation to modify worker's profile.
 * @param workerId Worker object id
 * @param refetch Refetch worker query
 * @param setIsDone SetState for isDone
 */
const getModifyUserMutation = (
	workerId: string,
	refetch: ApolloQueryRefetch,
	setIsDone: Dispatch<SetStateAction<boolean>>,
	authState: IAuthState,
	updateUser: (w: Partial<IWorker>) => void
) => {
	const history = useHistory();

	const [modifyUserMutation, { loading }] = useMutation(
		GraphqlWorker.worker_modify_data,
		{
			onCompleted: data => {
				toast.success(AlertMessages.USER_MODIFY_SUCCESS);
				if (workerId === authState.worker?._id)
					updateUser({ ...data.worker_modify_data });
				setIsDone(true);
				refetch();
				history.push(MainPaths.WORKERS.replace(MainParams.WORKER_ID, workerId));
			},
			onError: (error: ApolloError) => {
				toast.error(error.message || AlertMessages.SERVER_ERROR);
			},
		}
	);

	return { modifyUserMutation, loading };
};

/**
 * Gets the formik data to build the form.
 * @param modifyUserMutation Graphql mutation
 * @param worker Worker data
 */
const getForm = (
	modifyUserMutation: ApolloMutation,
	worker: IWorker
): FormikConfig<IWorkerDataInput> => {
	const initialValues: IWorkerDataInput = {
		[WorkerDataFields.EMAIL]: worker.email,
		[WorkerDataFields.NAME]: worker.name,
		[WorkerDataFields.SURNAME]: worker.surname,
		[WorkerDataFields.DISPLAY_NAME]: worker.displayName,
		[WorkerDataFields.USERNAME]: worker.username || '',
		[WorkerDataFields.BIO]: worker.bio || '',
		[WorkerDataFields.PHONE]: worker.phone || '',
	};

	const validationSchema = YupObject().shape({
		[WorkerDataFields.EMAIL]: YupString().test(
			'modify.email',
			FormMessages.EMAIL_ERROR,
			(value: any) => FormValidation.emailValidation(value || '')
		),
		[WorkerDataFields.NAME]: YupString().test(
			'modify.name',
			FormMessages.NAME_ERROR,
			(value: any) => FormValidation.nameValidation(value || '')
		),
		[WorkerDataFields.SURNAME]: YupString().test(
			'modify.surname',
			FormMessages.SURNAME_ERROR,
			(value: any) => FormValidation.nameValidation(value || '')
		),
		[WorkerDataFields.DISPLAY_NAME]: YupString().test(
			'modify.displayname',
			FormMessages.DISPLAYNAME_ERROR,
			(value: any) => FormValidation.nameValidation(value || '')
		),
		[WorkerDataFields.USERNAME]: YupString().test(
			'modify.username',
			FormMessages.USERNAME_ERROR,
			(value: any) =>
				value ? FormValidation.usernameValidation(value || '') : true
		),
		[WorkerDataFields.BIO]: YupString().test(
			'modify.bio',
			FormMessages.BIO_ERROR,
			(value: any) => (value ? FormValidation.bioValidation(value || '') : true)
		),

		[WorkerDataFields.PHONE]: YupString().test(
			'modify.bio',
			FormMessages.PHONE_ERROR,
			(value: any) =>
				value ? FormValidation.phoneValidation(value || '') : true
		),
	});

	const onSubmit = (values: IWorkerDataInput) => {
		const input: Partial<IWorkerDataInput> = {};

		Object.keys(values).forEach((key: any) => {
			// @ts-ignore
			if (values[key] !== initialValues[key]) {
				// @ts-ignore
				input[key] = values[key];
			}
		});

		if (!Object.keys(input).length)
			toast.error(AlertMessages.NOTHING_TO_MODIFY);
		else
			modifyUserMutation({
				variables: { workerId: worker._id, input },
			});
	};

	return {
		initialValues,
		validationSchema,
		onSubmit,
		validateOnChange: false,
	};
};

export default PageWorkerData;
