import { MainPaths } from '@Enums/paths/main-paths.enum';
import { AuthContext } from '@Lib/context/auth.context';
import { FC, useContext } from 'react';
import { Redirect } from 'react-router-dom';

export const withNoAuth = (Component: FC<any>) => (props: any) => {
	const { authState } = useContext(AuthContext);
	if (!authState.jwt) return <Component {...props} />;
	return <Redirect to={MainPaths.DASHBOARD} />;
};
