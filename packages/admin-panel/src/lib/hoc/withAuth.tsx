import Error404 from '@Components/other/error-404';
import Loader from '@Components/other/loader';
import { MainPaths } from '@Enums/paths/main-paths.enum';
import { WorkerRoles } from '@Enums/worker/worker-roles.enum';
import { AuthContext } from '@Lib/context/auth.context';
import { FC, ReactElement, useContext, useEffect, useState } from 'react';
import { Redirect } from 'react-router-dom';

export const withAuth = (
	Component: FC<any>,
	allowedRoles: WorkerRoles[] = []
) => (props: any) => {
	const { authState } = useContext(AuthContext);

	const [component, setComponent] = useState<ReactElement>(<Loader />);

	useEffect(() => {
		if (!authState.jwt) {
			setComponent(<Redirect push to={MainPaths.LOGIN} />);
			return;
		}

		if (authState.worker === undefined) return;
		else if (
			allowedRoles.length &&
			!allowedRoles.some(role => authState.worker?.roles.includes(role))
		)
			setComponent(<Error404 />);
		else setComponent(<Component {...props} />);
	}, [authState]);

	return component;
};
