import { InfoIcon } from '@Icons/courses/info-icon';
import { VideoIcon } from '@Icons/courses/video-icon';
import { LessonTypes } from '@Enums/courses/lesson-types.enum';
import {
	InfoLessonPublic,
	VideoLessonPublic,
} from '@Interfaces/course/lesson.interface';
import { FC } from 'react';

export type LessonCourseProps = {
	lesson: VideoLessonPublic | InfoLessonPublic;
};

const LessonCourse: FC<LessonCourseProps> = ({ lesson }) => {
	let lessonType;

	if (lesson.__typename === LessonTypes.INFO) {
		lessonType = (
			<div className='flex-c-c'>
				<InfoIcon className='h-1_5 pr-1 text-white-dark dark:text-white fill-current' />
				<h4 className='text-white-dark dark:text-white'>{lesson.title}</h4>
			</div>
		);
	} else {
		const duration = formatDuration(lesson.duration);

		lessonType = (
			<>
				<div className='flex-c-c'>
					<VideoIcon className='h-1_5 pr-1 text-white-dark dark:text-white fill-current' />
					<h4 className='text-white-dark dark:text-white'>{lesson.title}</h4>
				</div>
				<p className='text-white-dark dark:text-white'>{duration}</p>
			</>
		);
	}

	return (
		<div className='px-2 py-0_75 bg-secondary dark:bg-gray-900 flex-sb-c flex-wrap w-full'>
			{lessonType}
		</div>
	);
};

const formatDuration = (duration: number): string => {
	const durationMinutes = duration / 60000;

	if (durationMinutes > 60) {
		return `${(durationMinutes / 60).toPrecision(1)}h ${(
			durationMinutes % 60
		).toPrecision(1)}m`;
	} else return `${durationMinutes.toPrecision(1)}m`;
};

export default LessonCourse;
