import { QueryResult, useQuery } from '@apollo/client';
import LinkButton from '@Components/generic/link-button';
import { CartIcon } from '@Components/icons';
import Loader from '@Components/loader';
import { MainPaths } from '@Enums/paths/main-paths.enum';
import { PublicCourse } from '@Interfaces/course/course.interface';
import { ShoppingCartState } from '@Interfaces/states/shoppingcart-context.interface';
import { ShoppingCartContext } from '@Lib/context/shoppingcart.context';
import { GraphqlCourse } from 'learnthis-utils';
import { FC, useContext, useState } from 'react';
import ShoppingCartItem from './shoppingcart-item';

const ShoppingPopover: FC = () => {
	const { shoppingCart, removeItemShoppingCart } = useContext(
		ShoppingCartContext
	);
	const [popover, setPopover] = useState<boolean>(false);
	const response = getCoursesInfo(shoppingCart);

	return (
		<div
			className='relative mr-1_5 h-full flex-c-c xssm:hidden'
			onMouseEnter={() => setPopover(true)}
			onMouseLeave={() => setPopover(false)}>
			<CartIcon className='h-1_75 fill-current text-white-dark dark:text-white' />
			{shoppingCart.courses && shoppingCart.courses.length > 0 && (
				<p className='bg-cta-hover text-xs py-0_125 px-0_375 rounded-full absolute text- bottom-0_5 right--0_25 text-white-dark dark:text-white'>
					{shoppingCart.courses?.length}
				</p>
			)}
			<div
				className={`absolute z100 top-3_875 right--0_5 overflow-y-hidden box-border transition-all-eio-250 shadow-xl w-20 ${
					popover ? 'opacity-1 max-h-60' : 'opacity-0 max-h-0'
				}`}
				onMouseEnter={() => setPopover(true)}
				onMouseLeave={() => setPopover(false)}>
				<PopComponent
					response={response}
					removeItemShoppingCart={removeItemShoppingCart}></PopComponent>
			</div>
		</div>
	);
};

const getCoursesInfo = (shoppingCart: ShoppingCartState): QueryResult => {
	const query = GraphqlCourse.course_public_find_by_url_array;

	const response = useQuery(query, {
		variables: { coursesUrls: shoppingCart.courses },
		ssr: false,
	});

	return response;
};

export type PopComponentProps = {
	response: QueryResult;
	removeItemShoppingCart: (courseId: string) => Promise<void>;
};

const PopComponent: FC<PopComponentProps> = ({
	response,
	removeItemShoppingCart,
}) => {
	const { data, loading } = response;
	let itemsComponent;

	if (loading)
		itemsComponent = (
			<div className='mt-1 bg-white dark:bg-white-dark '>
				<div className='flex-c-c'>
					<Loader />
				</div>
			</div>
		);
	else if (data.course_public_find_by_url_array.length > 0) {
		itemsComponent = data.course_public_find_by_url_array.map(
			(course: PublicCourse) => (
				<ShoppingCartItem
					key={course._id}
					course={course}
					removeItemShoppingCart={removeItemShoppingCart}
				/>
			)
		);
		itemsComponent.push(
			<div key={-1} className='flex-c-c relative py-0_75 px-1'>
				<LinkButton kind='cta' href={MainPaths.CHECKOUT}>
					Checkout
				</LinkButton>
			</div>
		);
	} else
		itemsComponent = (
			<p className='text-center font-semibold px-0_5 py-1 text-white-dark dark:text-white'>
				No hay productos en el carrito
			</p>
		);

	return (
		<div className='mt-1 bg-white dark:bg-white-dark'>{itemsComponent}</div>
	);
};

export default ShoppingPopover;
