import ImgFallback from '@Components/generic/img-fallback';
import { TrashIcon } from '@Components/icons/menu/trash-icon';
import { PublicCourse } from '@Interfaces/course/course.interface';
import { FC } from 'react';

export type ShoppingCartItemProps = {
	course: PublicCourse;
	removeItemShoppingCart: (courseId: string) => Promise<void>;
};

const ShoppingCartItem: FC<ShoppingCartItemProps> = ({
	course,
	removeItemShoppingCart,
}) => {
	return (
		<div key={course._id} className='flex-s-c relative py-0_75 px-1'>
			<ImgFallback
				className='h-2_25 mr-1'
				src={course.image}
				fallbackSrc='/static/android-chrome-192x192.png'
			/>
			<div className='flexcol-s-s'>
				<h4 className='truncate-1-lines text-white-dark dark:text-white'>
					{course.title}
				</h4>
				<p className='truncate-1-lines text-sm text-white-dark dark:text-white'>
					{course.description.substring(0, 25) + '...'}
				</p>
			</div>
			<div
				className='absolute top-0_5 right-0_5 cursor-pointer text-red-500'
				onClick={() => removeItemShoppingCart(course._id)}>
				<TrashIcon className='h-1_25' />
			</div>
			<div className='absolute right-0_5 bottom-0_25 flex-c-c'>
				<span className='font-semibold text-white-dark dark:text-white'>
					{course.price}€
				</span>
			</div>
		</div>
	);
};

export default ShoppingCartItem;
