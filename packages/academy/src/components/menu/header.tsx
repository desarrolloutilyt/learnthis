import LinkButton from '@Components/generic/link-button';
import SwitchDarkMode from '@Components/generic/switch-dark-mode';
import { CartIcon } from '@Components/icons';
import { ThemeEnum } from '@Enums/config/theme.enum';
import { MainPaths } from '@Enums/paths/main-paths.enum';
import { LogoTextIcon } from '@Icons/logo/logo-text-icon';
import { MenuIcon } from '@Icons/menu/menu-icon';
import { StudentProfile } from '@Interfaces/student/student.interface';
import { menuController } from '@ionic/core';
import { BrowserContext } from '@Lib/context/resolution.context';
import dynamic from 'next/dynamic';
import Link from 'next/link';
import { FC, useContext } from 'react';
import MenuPopover from './menu-popover';

export type HeaderProps = {
	student?: StudentProfile;
};

const ShoppingPopover = dynamic(
	() => import('./shoppingcart/shopping-popover'),
	{
		ssr: false,
		loading: () => (
			<div className='relative mr-1_5 h-full flex-c-c xssm:hidden'>
				<CartIcon className='h-1_75 fill-current text-white-dark dark:text-white' />
			</div>
		),
	}
);

const Header: FC<HeaderProps> = ({ student }) => {
	const { browserPreferences, toggleDarkMode } = useContext(BrowserContext);

	return (
		<header className='appBar relative bg-white dark:bg-white-dark'>
			<div className='container-xl flex-c-c px-1 h-4'>
				<div className='flex-s-c flex-1'>
					<Link href='/'>
						<a>
							<LogoTextIcon className='h-2_75' />
						</a>
					</Link>
					<div className='xssm:hidden flex-c-c px-2'>
						<Link href={MainPaths.COURSES}>
							<a className='px-0_5 py-0_25 font-semibold text-white-dark dark:text-white'>
								Cursos
							</a>
						</Link>
						<Link href='/perfil/cursos'>
							<a className='px-0_5 py-0_25 font-semibold text-white-dark dark:text-white'>
								Sobre nosotros
							</a>
						</Link>
						<Link href='/contacto'>
							<a className='px-0_5 py-0_25 font-semibold text-white-dark dark:text-white'>
								Contacto
							</a>
						</Link>
					</div>
				</div>
				<div className='relative flex-e-c flex-1 h-full'>
					<SwitchDarkMode
						onChange={toggleDarkMode}
						checked={browserPreferences.theme === ThemeEnum.DARK}
					/>
					<ShoppingPopover />
					{student ? (
						<MenuPopover />
					) : (
						<LinkButton className='xssm:hidden' href='/login' kind='primary'>
							Inicia sesión
						</LinkButton>
					)}
					<MenuIcon
						className='mdlg:hidden h-1_75 fill-current text-white-dark dark:text-white'
						onClick={() => menuController.toggle('menuMob')}
					/>
				</div>
			</div>
		</header>
	);
};

export default Header;
