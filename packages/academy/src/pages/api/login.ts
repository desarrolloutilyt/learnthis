import { ApolloError } from '@apollo/client';
import { AlertMessages, FormMessages } from '@Enums/config/constants';
import { HTTPStatusCodes } from '@Enums/config/http-status-codes.enum';
import { createApolloClient } from '@Lib/apollo/apollo-client';
import { setJwtCookie } from '@Lib/login/jwt-cookie.utils';
import { FormValidation, GraphqlStudent } from 'learnthis-utils';
import { NextApiRequest, NextApiResponse } from 'next';

const login = async (req: NextApiRequest, res: NextApiResponse) => {
	if (req.method !== 'POST') {
		res.status(HTTPStatusCodes.METHOD_NOT_ALLOWED).send(false);
		return;
	}

	const { email, password } = req.body;

	//#region Validate input

	if (!email) {
		res.status(HTTPStatusCodes.BAD_REQUEST).send(FormMessages.EMAIL_REQUIRED);
		return;
	}

	if (!FormValidation.emailValidation(email)) {
		res.status(HTTPStatusCodes.BAD_REQUEST).send(FormMessages.EMAIL_ERROR);
		return;
	}

	if (!password) {
		res
			.status(HTTPStatusCodes.BAD_REQUEST)
			.send(FormMessages.PASSWORD_REQUIRED);
		return;
	}

	if (!FormValidation.passwordValidation(password)) {
		res.status(HTTPStatusCodes.UNAUTHORIZED).send(AlertMessages.INVALID_LOGIN);
		return;
	}

	//#endregion

	const client = createApolloClient();

	try {
		let response = await client.query({
			fetchPolicy: 'network-only',
			query: GraphqlStudent.student_login,
			variables: {
				input: {
					email,
					password,
				},
			},
		});

		const token = response.data.student_login.token;
		const user = response.data.student_login.user;

		if (token && user) {
			setJwtCookie(res, token);
			res.status(HTTPStatusCodes.OK).json({ token, user });
		}
	} catch (error: any) {
		if (error instanceof ApolloError && error?.graphQLErrors[0]) {
			res
				.status(error.graphQLErrors[0].extensions?.exception?.status)
				.send({ error: error.message });
		} else {
			res
				.status(HTTPStatusCodes.INTERNAL_SERVER_ERROR)
				.send({ error: AlertMessages.SERVER_ERROR });
		}
	}
};

export default login;
